/*
 * uartport.c
 *
 * Created: 2/22/2018 11:17:00 AM
 *  Author: Jake
 */ 

#include "uartport.h"
#include "hardware.h"

void uart_build(uartport_t *uart, Uart *com, Pio *port, uint32_t pinrx, uint32_t pintx, ringbuffer_t *rbrx, ringbuffer_t *rbtx, pin_t *stlr, pin_t *stlb){	
	uart->com = com;
	uart->port = port;
	
	uart->pinrx = pinrx;
	uart->pinrx_bm = 1 << pinrx;
	uart->pintx = pintx;
	uart->pintx_bm = 1 << pintx;
	
	uart->stlr = stlr;
	uart->stlb = stlb;
	
	uart->rbrx = rbrx;
	uart->rbtx = rbtx;
}

void uart_init(uartport_t *uart, uint32_t baud, uint32_t abcdsr){
	// PIO disable
	uart->port->PIO_PDR = uart->pinrx_bm | uart->pintx_bm;
	
	// abcdsr A[0,0], B[1,0], C[0,1], D[1,1]
	if(abcdsr == UART_IS_PERIPHERAL_B){
		uart->port->PIO_ABCDSR[0] |= uart->pinrx_bm | uart->pintx_bm;
	} else if (abcdsr == UART_IS_PERIPHERAL_C || abcdsr == UART_IS_PERIPHERAL_D){
		uart->port->PIO_ABCDSR[1] |= uart->pinrx_bm | uart->pintx_bm;
	}
	if (abcdsr == UART_IS_PERIPHERAL_D){
		uart->port->PIO_ABCDSR[0] |= uart->pinrx_bm | uart->pintx_bm;
	}
	
	// UART Registers
	// mode register
	uart->com->UART_MR = UART_MR_BRSRCCK_PERIPH_CLK | UART_MR_CHMODE_NORMAL | UART_MR_PAR_NO | UART_MR_FILTER_DISABLED;
	// baud: freq ?
	uart->com->UART_BRGR = baud;
	// channel enable
	uart->com->UART_CR = UART_CR_TXEN | UART_CR_RXEN;
	// interrupt enable (rx only, tx on data)
	uart->com->UART_IER = UART_IER_RXRDY;
	
	rb_reset(uart->rbrx);
	rb_reset(uart->rbtx);
	
	pin_set(uart->stlr);
	pin_set(uart->stlb);
}

void uart_sendchar_polled(uartport_t *uart, uint8_t data){
	while(!(uart->com->UART_SR & UART_SR_TXRDY));
	uart->com->UART_THR = data;
}

void uart_sendchar_buffered(uartport_t *uart, uint8_t data){
	rb_putchar(uart->rbtx, data);
	pin_clear(uart->stlb);
	uart->com->UART_IER = UART_IER_TXRDY;
}

void uart_sendchars_buffered(uartport_t *uart, uint8_t *data, uint8_t length){
	rb_putdata(uart->rbtx, data, length);
	pin_clear(uart->stlb);
	uart->com->UART_IER = UART_IER_TXRDY;
}

void uart_handler(uartport_t *uart){
	if(uart->com->UART_SR & UART_SR_RXRDY){
		uart_rxhandler(uart);
	}
	if((uart->com->UART_SR & UART_SR_TXRDY) && (uart->com->UART_IMR & UART_IMR_TXRDY)){
		uart_txhandler(uart);
	}
}

void uart_rxhandler(uartport_t *uart){
	pin_clear(uart->stlr); // set with whatever reads it (apa)
	//uint8_t data = uart->com->UART_RHR;
	rb_putchar(uart->rbrx, uart->com->UART_RHR);
	//uart_sendchar_buffered(ups[0], data);
}

void uart_txhandler(uartport_t *uart){
	if(!rb_empty(uart->rbtx)){
		uart->com->UART_THR = rb_get(uart->rbtx); // transmit if non-empty
	} else {
		pin_set(uart->stlb);
		uart->com->UART_IDR = UART_IDR_TXRDY; // or turn this interrupt off
	}
}
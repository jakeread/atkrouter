/*
 * apahandler.h
 *
 * Created: 3/12/2018 11:55:40 AM
 *  Author: Jake
 */ 


#ifndef APAHANDLER_H_
#define APAHANDLER_H_

#include <asf.h>

void apa_handle_packet(uint8_t *packet, uint8_t length);
void apa_return_packet(uint8_t *packet, uint8_t length);

#endif /* APAHANDLER_H_ */
/*
 * uartport.h
 *
 * Created: 2/22/2018 11:16:41 AM
 *  Author: Jake
 */ 


#ifndef UARTPORT_H_
#define UARTPORT_H_

#include <asf.h>
#include "ringbuffer.h"
#include "pin.h"

#define UART_IS_PERIPHERAL_A 0x00
#define UART_IS_PERIPHERAL_B 0x01
#define UART_IS_PERIPHERAL_C 0x02
#define UART_IS_PERIPHERAL_D 0x03

typedef struct{
	Uart *com;
	Pio *port;
		
	uint32_t pinrx;
	uint32_t pinrx_bm;
	uint32_t pintx;
	uint32_t pintx_bm;
	
	pin_t *stlr;
	pin_t *stlb;
	
	ringbuffer_t *rbrx;
	ringbuffer_t *rbtx;
}uartport_t;

void uart_build(uartport_t *uart, Uart *com, Pio *port, uint32_t pinrx, uint32_t pintx, ringbuffer_t *rbrx, ringbuffer_t *rbtx, pin_t *stlr, pin_t *stlb);

void uart_init(uartport_t *uart, uint32_t baud, uint32_t abcdsr);

void uart_sendchar_polled(uartport_t *uart, uint8_t data);
void uart_sendchar_buffered(uartport_t *uart, uint8_t data);
void uart_sendchars_buffered(uartport_t *uart, uint8_t *data, uint8_t length);

void uart_handler(uartport_t *uart);
void uart_rxhandler(uartport_t *uart);
void uart_txhandler(uartport_t *uart);

#endif /* UARTPORT_H_ */
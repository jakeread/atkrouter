/*
TODO: doc
*/

#include <asf.h>

#include "hardware.h"

void setupperipherals(void){
	// kill the watchdog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	// start relevant peripheral clocks
	PMC->PMC_PCER0 = 1 << ID_PIOA;
	PMC->PMC_PCER0 = 1 << ID_PIOD;
	
	// start uart clocks
	PMC->PMC_PCER0 = 1 << ID_UART0;
	PMC->PMC_PCER0 = 1 << ID_UART1;
	PMC->PMC_PCER1 = 1 << (ID_UART2 - 32);
	PMC->PMC_PCER1 = 1 << (ID_UART3 - 32);
	PMC->PMC_PCER1 = 1 << (ID_UART4 - 32);
}

void setupinterrupts(void){
	NVIC_DisableIRQ(UART0_IRQn);
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_SetPriority(UART0_IRQn, 8);
	NVIC_EnableIRQ(UART0_IRQn);
	
	NVIC_DisableIRQ(UART1_IRQn);
	NVIC_ClearPendingIRQ(UART1_IRQn);
	NVIC_SetPriority(UART1_IRQn, 8);
	NVIC_EnableIRQ(UART1_IRQn);
	
	NVIC_DisableIRQ(UART2_IRQn);
	NVIC_ClearPendingIRQ(UART2_IRQn);
	NVIC_SetPriority(UART2_IRQn, 8);
	NVIC_EnableIRQ(UART2_IRQn);
	
	NVIC_DisableIRQ(UART3_IRQn);
	NVIC_ClearPendingIRQ(UART3_IRQn);
	NVIC_SetPriority(UART3_IRQn, 8);
	NVIC_EnableIRQ(UART3_IRQn);
	
	NVIC_DisableIRQ(UART4_IRQn);
	NVIC_ClearPendingIRQ(UART4_IRQn);
	NVIC_SetPriority(UART4_IRQn, 8);
	NVIC_EnableIRQ(UART4_IRQn);
}

void lightsetup(void){
	pin_init(&up0_stlr, PIOA, PIO_PER_P2);
	pin_output(&up0_stlr);
	pin_init(&up0_stlb, PIOD, PIO_PER_P12);
	pin_output(&up0_stlb);
	
	pin_init(&up1_stlr, PIOD, PIO_PER_P20);
	pin_output(&up1_stlr);
	pin_init(&up1_stlb, PIOA, PIO_PER_P11);
	pin_output(&up1_stlb);
	
	pin_init(&up2_stlr, PIOA, PIO_PER_P15);
	pin_output(&up2_stlr);
	pin_init(&up2_stlb, PIOD, PIO_PER_P27);
	pin_output(&up2_stlb);
	
	pin_init(&up3_stlr, PIOA, PIO_PER_P22);
	pin_output(&up3_stlr);
	pin_init(&up3_stlb, PIOA, PIO_PER_P8);
	pin_output(&up3_stlb);
	
	pin_init(&up4_stlr, PIOA, PIO_PER_P18);
	pin_output(&up4_stlr);
	pin_init(&up4_stlb, PIOA, PIO_PER_P19);
	pin_output(&up4_stlb);
	
	pin_init(&stlticker, PIOD, PIO_PER_P13);
	pin_output(&stlticker);
	pin_set(&stlticker);
	
	pin_init(&stlpacket, PIOD, PIO_PER_P14);
	pin_output(&stlpacket);
	pin_set(&stlpacket);
	
	pin_init(&stlerr, PIOD, PIO_PER_P16);
	pin_output(&stlerr);
	pin_set(&stlerr);
}

void lightstoggle(void){
	for(int i = 0; i < 13; i++){
		pin_toggle(lights[i]);
	}
}

void initports(void){
	// RBs 1
	rb_init(&up0_rbrx);
	rb_init(&up0_rbtx);
	// UP1 on UART0, RX 9 TX 10 on PIOA
	uart_build(&up0, UART0, PIOA, 9, 10, &up0_rbrx, &up0_rbtx, &up0_stlr, &up0_stlb);
	uart_init(&up0, 81, UART_IS_PERIPHERAL_A);
	
	// RBs 2
	rb_init(&up1_rbrx);
	rb_init(&up1_rbtx);
	// UP2 on UART1, RX 5 TX 4 on PIOA
	uart_build(&up1, UART1, PIOA, 5, 4, &up1_rbrx, &up1_rbtx, &up1_stlr, &up1_stlb);
	uart_init(&up1, 81, UART_IS_PERIPHERAL_C); // 81 for FTDI 115200 :|

	// RBs 3
	rb_init(&up2_rbrx);
	rb_init(&up2_rbtx);
	// UP3 on UART2, RX 25 TX 26 on PIOD
	uart_build(&up2, UART2, PIOD, 25, 26, &up2_rbrx, &up2_rbtx, &up2_stlr, &up2_stlb);
	uart_init(&up2, 81, UART_IS_PERIPHERAL_C);
	
	// RBs 4
	rb_init(&up3_rbrx);
	rb_init(&up3_rbtx);
	// UP4 on UART3, RX 28 TX 30 on PIOD
	uart_build(&up3, UART3, PIOD, 28, 30, &up3_rbrx, &up3_rbtx, &up3_stlr, &up3_stlb);
	uart_init(&up3, 81, UART_IS_PERIPHERAL_A);
	
	// RBs 5
	rb_init(&up4_rbrx);
	rb_init(&up4_rbtx);
	// UP5 on UART4, RX 18 TX 19 on PIOD
	uart_build(&up4, UART4, PIOD, 18, 19, &up4_rbrx, &up4_rbtx, &up4_stlr, &up4_stlb);
	uart_init(&up4, 81, UART_IS_PERIPHERAL_C);
}

void listsetup(void){
	// array of ptrs to lights
	lights[0] = &up0_stlr;
	lights[1] = &up0_stlb;
	lights[2] = &up1_stlr;
	lights[3] = &up1_stlb;
	lights[4] = &up2_stlr;
	lights[5] = &up2_stlb;
	lights[6] = &up3_stlr;
	lights[7] = &up3_stlb;
	lights[8] = &up4_stlr;
	lights[9] = &up4_stlb;
	lights[10] = &stlticker;
	lights[11] = &stlpacket;
	lights[12] = &stlerr;

	// array of ptrs to uarts
	ups[0] = &up0;
	ups[1] = &up1;
	ups[2] = &up2;
	ups[3] = &up3;
	ups[4] = &up4;
}

int main (void){
	sysclk_init();
	board_init();
	setupperipherals();
	setupinterrupts();
	lightsetup();
	initports();
	listsetup();
	
	SysTick_Config(8000000);
	
	apaport_build(&apap0, 0, &up0, &up0_stlr, &up0_stlb);
	apaport_reset(&apap0);
	apaport_build(&apap1, 1, &up1, &up1_stlr, &up1_stlb);
	apaport_reset(&apap1);
	apaport_build(&apap2, 2, &up2, &up2_stlr, &up2_stlb);
	apaport_reset(&apap2);
	apaport_build(&apap3, 3, &up3, &up3_stlr, &up3_stlb);
	apaport_reset(&apap3);
	apaport_build(&apap4, 4, &up4, &up4_stlr, &up4_stlb);
	apaport_reset(&apap4);
	
	for(int i = 0; i < 8; i ++){
		lightstoggle();
		delay_cycles(3000000);
	}
	
	
	while(1){
		apaport_scan(&apap0, 2);
		apaport_scan(&apap1, 2);
		apaport_scan(&apap2, 2);
		apaport_scan(&apap3, 2);
		apaport_scan(&apap4, 2);
		delay_cycles(1);
	}
}

void SysTick_Handler(void){
	pin_toggle(&stlticker);
}

void UART0_Handler(void){
	uart_handler(&up0);
}

void UART1_Handler(void){
	uart_handler(&up1);
}

void UART2_Handler(void){
	uart_handler(&up2);
}

void UART3_Handler(void){
	uart_handler(&up3);
}

void UART4_Handler(void){
	uart_handler(&up4);
}
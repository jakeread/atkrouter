/*
 * hardware.h
 *
 * Created: 2/21/2018 6:28:28 PM
 *  Author: Jake
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "pin.h"
#include "ringbuffer.h"
#include "uartport.h"
#include "apaport.h"

// status lights
pin_t up0_stlr;
pin_t up0_stlb;

pin_t up1_stlr;
pin_t up1_stlb;

pin_t up2_stlr;
pin_t up2_stlb;

pin_t up3_stlr;
pin_t up3_stlb;

pin_t up4_stlr;
pin_t up4_stlb;

pin_t stlticker;
pin_t stlerr;
pin_t stlpacket;

// array of ptrs to lights
// init in main.c
#define NUM_LIGHTS 13
pin_t *lights[NUM_LIGHTS];

// ringbuffers
ringbuffer_t up0_rbrx;
ringbuffer_t up0_rbtx;
ringbuffer_t up1_rbrx;
ringbuffer_t up1_rbtx;
ringbuffer_t up2_rbrx;
ringbuffer_t up2_rbtx;
ringbuffer_t up3_rbrx;
ringbuffer_t up3_rbtx;
ringbuffer_t up4_rbrx;
ringbuffer_t up4_rbtx;

// uartports
uartport_t up0;
uartport_t up1;
uartport_t up2;
uartport_t up3;
uartport_t up4;

// array of ptrs to uartports
// inits in main.c
#define NUM_UPS 5
uartport_t *ups[NUM_UPS];

// apaports
apaport_t apap0;
apaport_t apap1;
apaport_t apap2;
apaport_t apap3;
apaport_t apap4;


#endif /* HARDWARE_H_ */
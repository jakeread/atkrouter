/*
 * pin.c
 *
 * Created: 11/23/2017 1:24:54 PM
 *  Author: Jake
 */ 

#include "pin.h"
#include <asf.h>

// don't return aggregates 
// https://bytes.com/topic/c/answers/644271-aggregate-return-warnings 

void pin_init(pin_t *pin, Pio *port, uint32_t pin_bitmask){
	pin->port = port;
	pin->pin_bm = pin_bitmask;
}

void pin_output(pin_t *pin){
	pin->port->PIO_PER |= pin->pin_bm;
	pin->port->PIO_OER = pin->pin_bm;
}

void pin_set(pin_t *pin){
	pin->port->PIO_SODR = pin->pin_bm;
}

void pin_clear(pin_t *pin){
	pin->port->PIO_CODR = pin->pin_bm;
}

void pin_toggle(pin_t *pin){
	if(pin->port->PIO_ODSR & pin->pin_bm){
		pin->port->PIO_CODR = pin->pin_bm;
	} else {
		pin->port->PIO_SODR = pin->pin_bm;
	}
}

void pin_input(pin_t *pin){
	pin->port->PIO_PER |= pin->pin_bm;
	pin->port->PIO_ODR = pin->pin_bm;
}

int pin_read(pin_t *pin){
	return pin->port->PIO_PDSR & pin->pin_bm;
}
/*
 * apahandler.c
 *
 * Created: 3/12/2018 11:55:30 AM
 *  Author: Jake
 */ 

#include "hardware.h"
#include "apahandler.h"

void apa_handle_packet(uint8_t *packet, uint8_t length){
	// ack with new packet
	apa_return_packet(packet, length);	
	// through packet
	for(int i = 0; i < length; i ++){
		if(packet[i] == APA_END_ROUTE_DELIMITER){
			// data begins
			// check we have data after addr delimiter
			if(i + 1 < length){
				int thelight = packet[i+1] % 10;
				pin_toggle(lights[thelight]);
				break;
			} else {
				break;
			}
		}
	}
}

void apa_return_packet(uint8_t *packet, uint8_t length){
	uint8_t ackpack[length];
	ackpack[0] = length;
	// find route header
	int i = 2;
	int stop = 0;
	while(i < length){
		if(packet[i] == APA_END_ROUTE_DELIMITER){
			stop = i;
			break;
		}
		i ++;
	}
	// do the business
	if(!stop){
		// error if stop == 0
	} else {
		// reverse the address header
		for(int a = stop - 1, b = 1; a >= 1; a--, b++){
			ackpack[b] = packet[a];
		}
		// fill the rest with same packet data
		ackpack[stop] = APA_END_ROUTE_DELIMITER;
		for(int u = stop; u < length; u ++){
			ackpack[u] = packet[u];
		}
		uart_sendchars_buffered(ups[ackpack[1]], ackpack, length);
	}
}
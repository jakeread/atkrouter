/*
 * hardware.h
 *
 * Created: 6/18/2018 12:18:05 PM
 *  Author: Jake
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "pin.h"
#include "ringbuffer.h"
#include "uartport.h"
#include "atkport.h"
#include "atkhandler.h"

// A: 5 B: 0 - 2uS bit, 
// A: 11 B: 0 - 4uS bit, 250kBaud
#define SYSTEM_BAUDA 11
#define SYSTEM_BAUDB 0
#define SYSTEM_NUM_UPS 7

pin_t stlclk;
pin_t stlerr;

// UP0

ringbuffer_t up0rbrx;
ringbuffer_t up0rbtx;

uartport_t up0;

pin_t up0rxled;
pin_t up0txled;

atkport_t atkp0;

// UP1

ringbuffer_t up1rbrx;
ringbuffer_t up1rbtx;

uartport_t up1;

pin_t up1rxled;
pin_t up1txled;

atkport_t atkp1;

// UP2

ringbuffer_t up2rbrx;
ringbuffer_t up2rbtx;

uartport_t up2;

pin_t up2rxled;
pin_t up2txled;

atkport_t atkp2;

// UP3

ringbuffer_t up3rbrx;
ringbuffer_t up3rbtx;

uartport_t up3;

pin_t up3rxled;
pin_t up3txled;

atkport_t atkp3;

// UP4

ringbuffer_t up4rbrx;
ringbuffer_t up4rbtx;

uartport_t up4;

pin_t up4rxled;
pin_t up4txled;

atkport_t atkp4;

// UP 5


ringbuffer_t up5rbrx;
ringbuffer_t up5rbtx;

uartport_t up5;

pin_t up5rxled;
pin_t up5txled;

atkport_t atkp5;

// UPS

// UP USB (UP6)

ringbuffer_t upUrbrx;
ringbuffer_t upUrbtx;

uartport_t upU;

pin_t upUrxled; // don't exist
pin_t upUtxled;

atkport_t atkp6;

uartport_t *ups[SYSTEM_NUM_UPS];

#endif /* HARDWARE_H_ */
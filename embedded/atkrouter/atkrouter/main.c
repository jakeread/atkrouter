/*
* atkrouter.c
*
* Created: 6/17/2018 2:48:08 PM
* Author : Jake
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "hardware.h"
#include "fastmath.h"

// stlclk PA0
// stlerr PA1

// do clock hotfix
// do uartports
// do cp2102n

void clock_init(void){
	OSC.XOSCCTRL = OSC_XOSCSEL_XTAL_256CLK_gc | OSC_FRQRANGE_12TO16_gc; // select external source
	OSC.CTRL = OSC_XOSCEN_bm; // enable external source
	while(!(OSC.STATUS & OSC_XOSCRDY_bm)); // wait for external
	OSC.PLLCTRL =  OSC_PLLSRC_XOSC_gc | 3; // select external osc for pll, do pll = source * 3
	OSC.CTRL |= OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL for main clock
	/*
	OSC.PLLCTRL = OSC_PLLFAC4_bm | OSC_PLLFAC3_bm; // 2 MHz * 24 = 48 MHz
	OSC.CTRL = OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL
	*/
}

#define ATKTIMER_MODE_MASTER 1
#define ATKTIMER_MODE_SLAVE 0 
#define ATKTIMER_MODE_RING 2

void atktimer_init(uint8_t mode){
	/*
	in this test, 
	tx: pin F0 outputs a 1MHz clock on all output lines
	rx: pin F4 (atk1-clkin) listens to both edges, outputs that event to PE7 (ATK1-tx)
	*/
	// BREAKS PF0 UP0 RXLED
	PORTF.DIRSET = PIN0_bm; // this is our clk output pin
	PORTF.PIN0CTRL = PORT_SRLEN_bm; // slew rate limit to decrease noise (a little bit)
	
	if(mode == ATKTIMER_MODE_MASTER){
		// put a 1MHz timer on the clock out lines
		// clkout pin is PF0, TCF0.CCA
		TCF0.CTRLA = TC_CLKSEL_DIV1_gc; // one tick is 48MHz
		
		uint16_t pera = 47; // going for 1MHz
		uint8_t peral = (uint8_t) pera;
		uint8_t perah = (uint8_t)(pera >> 8);
		
		TCF0.CTRLB = TC_WGMODE_FRQ_gc | (1 << 4); // frequency wave mode, and enable CCA channel output
		// need to set CCA, 
		TCF0.CCABUFL = peral;
		TCF0.CCABUFH = perah;
	} else if(mode == ATKTIMER_MODE_SLAVE) {
		// listen for clock on a port, let's say ATK1 for now, and mirror that on our clock outputs
		PORTF.DIRCLR = PIN4_bm; // this is ATK1-CLKIN
		PORTF.PIN4CTRL = PORT_ISC_BOTHEDGES_gc; // sense when falling
		
		// setup event channel 0 to have EVSYS_CHMUX_PORTD_PIN4_gc as input
		EVSYS_CH0MUX = EVSYS_CHMUX_PORTF_PIN4_gc;
		// it seems we can only output events directly on pin7 of ports c, d or e :| (or pin 4 on the same ports)
		// so output event 0 on port E, pin 7
		// this will put event channel 0 on a pin, directly - only for as many clock cycles as the event is active. i.e. one
		//PORTCFG_CLKEVOUT = PORTCFG_EVOUT_PE7_gc;
		
		TCF0.CTRLA = TC_CLKSEL_EVCH0_gc;
		
		TCF0.CTRLB = TC_WGMODE_FRQ_gc | (1 << 4); // frequency wave mode, and enable CCA channel output
		// need to set CCA,
		TCF0.CCABUFL = 0;
		TCF0.CCABUFH = 0;
	}
}

void uarts_init(void){
	// UP0
	rb_init(&up0rbrx);
	rb_init(&up0rbtx);
	pin_init(&up0rxled, &PORTF, PIN0_bm, 0, 1);
	pin_init(&up0txled, &PORTF, PIN1_bm, 1, 1);
	uart_init(&up0, &USARTF0, &PORTF, PIN2_bm, PIN3_bm, &up0rbrx, &up0rbtx, &up0rxled, &up0txled);
	uart_start(&up0, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	// UP1
	rb_init(&up1rbrx);
	rb_init(&up1rbtx);
	pin_init(&up1rxled, &PORTE, PIN4_bm, 4, 1);
	pin_init(&up1txled, &PORTE, PIN5_bm, 5, 1);
	uart_init(&up1, &USARTE1, &PORTE, PIN6_bm, PIN7_bm, &up1rbrx, &up1rbtx, &up1rxled, &up1txled);
	uart_start(&up1, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	// UP2
	rb_init(&up2rbrx);
	rb_init(&up2rbtx);
	pin_init(&up2rxled, &PORTE, PIN0_bm, 0, 1);
	pin_init(&up2txled, &PORTE, PIN1_bm, 1, 1);
	uart_init(&up2, &USARTE0, &PORTE, PIN2_bm, PIN3_bm, &up2rbrx, &up2rbtx, &up2rxled, &up2txled);
	uart_start(&up2, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	// UP3
	rb_init(&up3rbrx);
	rb_init(&up3rbtx);
	pin_init(&up3rxled, &PORTD, PIN4_bm, 4, 1);
	pin_init(&up3txled, &PORTD, PIN5_bm, 5, 1);
	uart_init(&up3, &USARTD1, &PORTD, PIN6_bm, PIN7_bm, &up3rbrx, &up3rbtx, &up3rxled, &up3txled);
	uart_start(&up3, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	// UP4
	rb_init(&up4rbrx);
	rb_init(&up4rbtx);
	pin_init(&up4rxled, &PORTD, PIN0_bm, 0, 1);
	pin_init(&up4txled, &PORTD, PIN1_bm, 1, 1);
	uart_init(&up4, &USARTD0, &PORTD, PIN2_bm, PIN3_bm, &up4rbrx, &up4rbtx, &up4rxled, &up4txled);
	uart_start(&up4, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	// UP5
	rb_init(&up5rbrx);
	rb_init(&up5rbtx);
	pin_init(&up5rxled, &PORTC, PIN4_bm, 4, 1);
	pin_init(&up5txled, &PORTC, PIN5_bm, 5, 1);
	uart_init(&up5, &USARTC1, &PORTC, PIN6_bm, PIN7_bm, &up5rbrx, &up5rbtx, &up5rxled, &up5txled);
	uart_start(&up5, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	// UP USB (UP6)
	rb_init(&upUrbrx);
	rb_init(&upUrbtx);
	pin_init(&upUrxled, &PORTC, PIN0_bm, 0, 1);
	pin_init(&upUtxled, &PORTC, PIN1_bm, 1, 1);
	uart_init(&upU, &USARTC0, &PORTC, PIN2_bm, PIN3_bm, &upUrbrx, &upUrbtx, &upUrxled, &upUtxled);
	uart_start(&upU, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	ups[0] = &up0;
	ups[1] = &up1;
	ups[2] = &up2;
	ups[3] = &up3;
	ups[4] = &up4;
	ups[5] = &up5;
	ups[6] = &upU;
}

void atkps_init(void){
	atkport_init(&atkp0, 0, &up0);
	atkport_init(&atkp1, 1, &up1);
	atkport_init(&atkp2, 2, &up2);
	atkport_init(&atkp3, 3, &up3);
	atkport_init(&atkp4, 4, &up4);
	atkport_init(&atkp5, 5, &up5);
	atkport_init(&atkp6, 6, &upU);
}

int main(void)
{
	clock_init();
	//atktimer_init(ATKTIMER_MODE_MASTER);
	uarts_init();
	atkps_init();
	
	// enable interrupts
	sei();
	PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

	pin_init(&stlclk, &PORTA, PIN0_bm, 0, 1);
	pin_init(&stlerr, &PORTA, PIN1_bm, 1, 1);
	pin_set(&stlerr);
	
	// runtime globals
	uint32_t tck = 0;

	while (1)
	{
		atkport_scan(&atkp0, 2);
		atkport_scan(&atkp1, 2);
		atkport_scan(&atkp2, 2);
		atkport_scan(&atkp3, 2);
		atkport_scan(&atkp4, 2);
		atkport_scan(&atkp5, 2);
		atkport_scan(&atkp6, 2);
				
		tck++;
		if(!fastModulo(tck, 4096)){
			pin_toggle(&stlclk);
		}
	}
}

ISR(USARTF0_RXC_vect){
	uart_rxhandler(&up0);
}

ISR(USARTF0_DRE_vect){
	uart_txhandler(&up0);
}

ISR(USARTE1_RXC_vect){
	uart_rxhandler(&up1);
}

ISR(USARTE1_DRE_vect){
	uart_txhandler(&up1);
}

ISR(USARTE0_RXC_vect){
	uart_rxhandler(&up2);
}

ISR(USARTE0_DRE_vect){
	uart_txhandler(&up2);
}

ISR(USARTD1_RXC_vect){
	uart_rxhandler(&up3);
}

ISR(USARTD1_DRE_vect){
	uart_txhandler(&up3);
}

ISR(USARTD0_RXC_vect){
	uart_rxhandler(&up4);
}

ISR(USARTD0_DRE_vect){
	uart_txhandler(&up4);
}

ISR(USARTC1_RXC_vect){
	uart_rxhandler(&up5);
}

ISR(USARTC1_DRE_vect){
	uart_txhandler(&up5);
}

ISR(USARTC0_RXC_vect){
	uart_rxhandler(&upU);
}

ISR(USARTC0_DRE_vect){
	uart_txhandler(&upU);
}
/*
 * usb-adafruit-cdc.c
 *
 * Created: 5/3/2018 6:39:05 PM
 * Author : Jake
 */ 

#include <stdio.h>
#include "sam.h"
#include "sam_ba_usb.h"
#include "sam_ba_monitor.h"

#include "hardware.h"

void lights_init(void){
	stlTicker = pin_new(0, 3);
	pin_output(&stlTicker);
	pin_set(&stlTicker);
	stlUsb = pin_new(0, 2);
	pin_output(&stlUsb);
	pin_set(&stlUsb);
	stlPacket = pin_new(1, 3);
	pin_output(&stlPacket);
	pin_set(&stlPacket);
	stlErr = pin_new(1, 2);
	pin_output(&stlErr);
	pin_set(&stlErr);
	
	up0stlb = pin_new(1, 22);
	pin_output(&up0stlb);
	pin_set(&up0stlb);
	up0stlr = pin_new(1, 31);
	pin_output(&up0stlr);
	pin_set(&up0stlr);
	
	up1stlb = pin_new(0, 20);
	pin_output(&up1stlb);
	pin_set(&up1stlb);
	up1stlr = pin_new(0, 21);
	pin_output(&up1stlr);
	pin_set(&up1stlr);
	
	up2stlb = pin_new(1, 16);
	pin_output(&up2stlb);
	pin_set(&up2stlb);
	up2stlr = pin_new(1, 17);
	pin_output(&up2stlr);
	pin_set(&up2stlr);
	
	up3stlb = pin_new(0, 18);
	pin_output(&up3stlb);
	pin_set(&up3stlb);
	up3stlr = pin_new(0, 19);
	pin_output(&up3stlr);
	pin_set(&up3stlr);
	
	up4stlb = pin_new(0, 14);
	pin_output(&up4stlb);
	pin_set(&up4stlb);
	up4stlr = pin_new(0, 15);
	pin_output(&up4stlr);
	pin_set(&up4stlr);
	
	up5stlb = pin_new(1, 13);
	pin_output(&up5stlb);
	pin_set(&up5stlb);
	up5stlr = pin_new(1, 14);
	pin_output(&up5stlr);
	pin_set(&up5stlr);
}

void lights_flash(void){
	// yikes!
	pin_toggle(&stlTicker);
	pin_toggle(&stlPacket);
	pin_toggle(&stlErr);
	pin_toggle(&up0stlb);
	pin_toggle(&up0stlr);
	pin_toggle(&up1stlb);
	pin_toggle(&up1stlr);
	pin_toggle(&up2stlb);
	pin_toggle(&up2stlr);
	pin_toggle(&up3stlb);
	pin_toggle(&up3stlr);
	pin_toggle(&up4stlb);
	pin_toggle(&up4stlr);
	pin_toggle(&up5stlb);
	pin_toggle(&up5stlr);
}

void uarts_init(void){
	// don't forget to also add the handler
	NVIC_EnableIRQ(SERCOM0_0_IRQn);
	NVIC_EnableIRQ(SERCOM0_2_IRQn);
	// init rbs
	rb_init(&up0rbrx);
	rb_init(&up0rbtx);
	// init uart 
	up0 = uart_new(SERCOM0, &PORT->Group[0], &up0rbrx, &up0rbtx, &up0stlr, &up0stlb, 5, 4);
	MCLK->APBAMASK.bit.SERCOM0_ = 1;
	uart_init(&up0, 7, SERCOM0_GCLK_ID_CORE, BAUD_SYSTEM, HARDWARE_ON_PERIPHERAL_D);
	
	
	// handlers
	NVIC_EnableIRQ(SERCOM1_0_IRQn);
	NVIC_EnableIRQ(SERCOM1_2_IRQn);
	// init rbs
	rb_init(&up1rbrx);
	rb_init(&up1rbtx);
	// init uart
	up1 = uart_new(SERCOM1, &PORT->Group[0], &up1rbrx, &up1rbtx, &up1stlr, &up1stlb, 1, 0);
	MCLK->APBAMASK.bit.SERCOM1_ = 1;
	uart_init(&up1, 7, SERCOM1_GCLK_ID_CORE, BAUD_SYSTEM, HARDWARE_ON_PERIPHERAL_D);
	
	
	// handlers
	NVIC_EnableIRQ(SERCOM2_0_IRQn);
	NVIC_EnableIRQ(SERCOM2_2_IRQn);
	// init rbs
	rb_init(&up2rbrx);
	rb_init(&up2rbtx);
	// init uart
	up2 = uart_new(SERCOM2, &PORT->Group[0], &up2rbrx, &up2rbtx, &up2stlr, &up2stlb, 8, 9);
	MCLK->APBBMASK.bit.SERCOM2_ = 1;
	uart_init(&up2, 7, SERCOM2_GCLK_ID_CORE, BAUD_SYSTEM, HARDWARE_ON_PERIPHERAL_D);
	
	
	// handlers
	NVIC_EnableIRQ(SERCOM3_0_IRQn);
	NVIC_EnableIRQ(SERCOM3_2_IRQn);
	// init rbs
	rb_init(&up3rbrx);
	rb_init(&up3rbtx);
	// init uart
	up3 = uart_new(SERCOM3, &PORT->Group[0], &up3rbrx, &up3rbtx, &up3stlr, &up3stlb, 16, 17);
	MCLK->APBBMASK.bit.SERCOM3_ = 1;
	uart_init(&up3, 7, SERCOM3_GCLK_ID_CORE, BAUD_SYSTEM, HARDWARE_ON_PERIPHERAL_D);
	
	
	// handlers
	NVIC_EnableIRQ(SERCOM4_0_IRQn);
	NVIC_EnableIRQ(SERCOM4_2_IRQn);
	// init rbs
	rb_init(&up4rbrx);
	rb_init(&up4rbtx);
	// init uart
	up4 = uart_new(SERCOM4, &PORT->Group[0], &up4rbrx, &up4rbtx, &up4stlr, &up4stlb, 12, 13);
	MCLK->APBDMASK.bit.SERCOM4_ = 1;
	uart_init(&up4, 7, SERCOM4_GCLK_ID_CORE, BAUD_SYSTEM, HARDWARE_ON_PERIPHERAL_D);
	
	
	// handlers
	NVIC_EnableIRQ(SERCOM5_0_IRQn);
	NVIC_EnableIRQ(SERCOM5_2_IRQn);
	// init rbs
	rb_init(&up5rbrx);
	rb_init(&up5rbtx);
	// uart init
	up5 = uart_new(SERCOM5, &PORT->Group[0], &up5rbrx, &up5rbtx, &up5stlr, &up5stlb, 22, 23);
	MCLK->APBDMASK.bit.SERCOM5_ = 1;
	uart_init(&up5, 7, SERCOM5_GCLK_ID_CORE, BAUD_SYSTEM, HARDWARE_ON_PERIPHERAL_D);
	
	
	// now pack all into ups
	ups[0] = &up0;
	ups[1] = &up1;
	ups[2] = &up2;
	ups[3] = &up3;
	ups[4] = &up4;
	ups[5] = &up5;
}

void apaports_init(void){
	apap0 = apaport_new(0, &up0, &up0stlr, &up0stlb);
	apaport_reset(&apap0);

	apap1 = apaport_new(1, &up1, &up1stlr, &up1stlb);
	apaport_reset(&apap1);
	
	apap2 = apaport_new(2, &up2, &up2stlr, &up2stlb);
	apaport_reset(&apap2);
	
	apap3 = apaport_new(3, &up3, &up3stlr, &up3stlb);
	apaport_reset(&apap3);
	
	apap4 = apaport_new(4, &up4, &up4stlr, &up4stlb);
	apaport_reset(&apap4);
	
	apap5 = apaport_new(5, &up5, &up5stlr, &up5stlb);
	apaport_reset(&apap5);
}

static volatile bool main_b_cdc_enable = false;

int main(void)
{
    /* Initialize the SAM system */
    SystemInit();
	// clock setup to run main CPU at 120MHz, and DFLL48M setup from internal osc, should run USB
	clock_init();
	// enable interrupts in the system
	__enable_irq();
	
	lights_init();
	
	// init uartports
	uarts_init();
	
	// init apaports
	apaports_init();
		
	// pointer to the USB struct in sam_ba_usb.h
	P_USB_CDC pCdc; 
	
	// turn the USB on, sam_ba_usb.h and .c
	pCdc = usb_init();
	
	// a ticker to look for hangouts
	SysTick_Config(8000000);
	
	for(int i = 0; i < 600000; i ++){
		if(!(i % 190000)){
			lights_flash();
		}
	}

    while (1) 
    {
		// waits for config to config
		if(pCdc->IsConfigured(pCdc) != 0){
			main_b_cdc_enable = true;
		}
				
		// if config is config, and port is opened
		if(main_b_cdc_enable){
			sam_ba_monitor_init(SAM_BA_INTERFACE_USBCDC);
			// loops on this
			while(1){
				sam_ba_monitor_run();
								
				apaport_scan(&apap0, 2);
				apaport_scan(&apap1, 2);
				apaport_scan(&apap2, 2);
				apaport_scan(&apap3, 2);
				apaport_scan(&apap4, 2);
				apaport_scan(&apap5, 2);
				
				/*
				while(!rb_empty(up0.rbrx)){
					uart_sendchar_buffered(&up0, rb_get(up0.rbrx));
				}
				
				while(!rb_empty(up1.rbrx)){
					uart_sendchar_buffered(&up1, rb_get(up1.rbrx));
				}
				
				while(!rb_empty(up2.rbrx)){
					uart_sendchar_buffered(&up2, rb_get(up2.rbrx));
				}
				
				while(!rb_empty(up3.rbrx)){
					uart_sendchar_buffered(&up3, rb_get(up3.rbrx));
				}
				
				while(!rb_empty(up4.rbrx)){
					uart_sendchar_buffered(&up4, rb_get(up4.rbrx));
				}
				
				while(!rb_empty(up5.rbrx)){
					uart_sendchar_buffered(&up5, rb_get(up5.rbrx));
				}
				*/
				//uart_sendchars_buffered(&up0, &testUart, 3);
				// apaport loops
			}
		} else {
			apaport_scan(&apap0, 2);
			apaport_scan(&apap1, 2);
			apaport_scan(&apap2, 2);
			apaport_scan(&apap3, 2);
			apaport_scan(&apap4, 2);
			apaport_scan(&apap5, 2);
		}
	}
}

void SysTick_Handler(void){
	pin_toggle(&stlTicker);
	
	/*
	pin_toggle(&stlPacket);
	pin_toggle(&stlErr);
	
	pin_toggle(&up0stlb);
	pin_toggle(&up0stlr);
	pin_toggle(&up1stlb);
	pin_toggle(&up1stlr);
	pin_toggle(&up2stlb);
	pin_toggle(&up2stlr);
	pin_toggle(&up3stlb);
	pin_toggle(&up3stlr);
	pin_toggle(&up4stlb);
	pin_toggle(&up4stlr);
	pin_toggle(&up5stlb);
	pin_toggle(&up5stlr);
	*/
	// monitor_sys_tick
}

void SERCOM0_0_Handler(void){
	uart_txhandler(&up0);
}

void SERCOM0_2_Handler(void){
	uart_rxhandler(&up0);
}

void SERCOM1_0_Handler(void){
	uart_txhandler(&up1);
}

void SERCOM1_2_Handler(void){
	uart_rxhandler(&up1);
}

void SERCOM2_0_Handler(void){
	uart_txhandler(&up2);
}

void SERCOM2_2_Handler(void){
	uart_rxhandler(&up2);
}

void SERCOM3_0_Handler(void){
	uart_txhandler(&up3);
}

void SERCOM3_2_Handler(void){
	uart_rxhandler(&up3);
}

void SERCOM4_0_Handler(void){
	uart_txhandler(&up4);
}

void SERCOM4_2_Handler(void){
	uart_rxhandler(&up4);
}

void SERCOM5_0_Handler(void){
	uart_txhandler(&up5);
}

void SERCOM5_2_Handler(void){
	uart_rxhandler(&up5);
}
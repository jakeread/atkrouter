/*
 * apahandler.c
 *
 * Created: 3/12/2018 11:55:30 AM
 *  Author: Jake
 */ 

#include "hardware.h"
#include "apahandler.h"

void apa_handle_packet(uint8_t *packet, uint8_t length){
	// dirty debug reply
	
	//sam_ba_port_put_data(packet, packet[0]);
		
	// through packet
	
	int i = 0;
	int apa_handler_state = APA_HANDLER_OUTSIDE;
	
	while(i < length){ // prep for the messy double switch :|
		switch (apa_handler_state){
			case APA_HANDLER_OUTSIDE:
				if (packet[i] == APA_END_ROUTE_DELIMITER){
					apa_handler_state = APA_HANDLER_INSIDE;
				} else {
					//
				}
				i ++;
				break;
				
			case APA_HANDLER_INSIDE:
				switch (packet[i]){
					
					case DELIM_KEY_TEST:
						apa_return_packet(packet, length);
						pin_toggle(&stlErr);
						i ++;
						break;
																
					default:
						// probably an error
						i ++;
						break;
				} // end interior switch
				break;
				
			default: 
				// throw err
				break;
		} // end y/n switch
	}
}


void apa_return_packet(uint8_t *packet, uint8_t length){
	//uart_sendchar_buffered(ups[1], 120);
	//uart_sendchars_buffered(ups[1], packet, length);
	uint8_t ackpack[length];
	ackpack[0] = length;
	// find route header
	int i = 2;
	int stop = 0;
	while(i < length){
		if(packet[i] == APA_END_ROUTE_DELIMITER){
			stop = i;
			break;
		}
		i ++;
	}
	// do the business
	if(!stop){
		// error if stop == 0
		} else {
		// reverse the address header
		for(int a = stop - 1, b = 1; a >= 1; a--, b++){
			ackpack[b] = packet[a];
		}
		// fill the rest with same packet data
		ackpack[stop] = APA_END_ROUTE_DELIMITER;
		for(int u = stop; u < length; u ++){
			ackpack[u] = packet[u];
		}
		uart_sendchars_buffered(ups[ackpack[1]], ackpack, length);
		// NOW:
		// looking for justice: why no return packet on double length hop?
		// debug with 2nd ftdi
		//uart_sendchar_buffered(ups[1], 121);
		//uart_sendchars_buffered(ups[1], ackpack, length);
	}
}
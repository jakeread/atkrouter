/*
Copyright (c) 2015 Arduino LLC.  All right reserved.
Copyright (c) 2015 Atmel Corporation/Thibaut VIARD.  All right reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "sam.h"
#include <string.h>
#include "sam_ba_monitor.h"
#include "sam_ba_serial.h"
#include "board_driver_serial.h"
#include "board_driver_usb.h"
#include "sam_ba_usb.h"
#include "sam_ba_cdc.h"
//#include "board_driver_led.h"

#include "hardware.h"

const char RomBOOT_Version[] = SAM_BA_VERSION;
const char RomBOOT_ExtendedCapabilities[] = "[Arduino:XYZ]";

//messy check for port open 

uint8_t usbComOpen = 0;

/* Provides one common interface to handle both USART and USB-CDC */
typedef struct
{
	/* send one byte of data */
	int (*put_c)(int value);
	/* Get one byte */
	int (*get_c)(void);
	/* Receive buffer not empty */
	bool (*is_rx_ready)(void);
	/* Send given data (polling) */
	uint32_t (*putdata)(void const* data, uint32_t length);
	/* Get data from comm. device */
	uint32_t (*getdata)(void* data, uint32_t length);
	/* Send given data (polling) using xmodem (if necessary) */
	uint32_t (*putdata_xmd)(void const* data, uint32_t length);
	/* Get data from comm. device using xmodem (if necessary) */
	uint32_t (*getdata_xmd)(void* data, uint32_t length);
} t_monitor_if;

#if SAM_BA_INTERFACE == SAM_BA_UART_ONLY  ||  SAM_BA_INTERFACE == SAM_BA_BOTH_INTERFACES
/* Initialize structures with function pointers from supported interfaces */
const t_monitor_if uart_if =
{
	.put_c =       serial_putc,
	.get_c =       serial_getc,
	.is_rx_ready = serial_is_rx_ready,
	.putdata =     serial_putdata,
	.getdata =     serial_getdata,
	.putdata_xmd = serial_putdata_xmd,
	.getdata_xmd = serial_getdata_xmd
};
#endif

#if SAM_BA_INTERFACE == SAM_BA_USBCDC_ONLY  ||  SAM_BA_INTERFACE == SAM_BA_BOTH_INTERFACES
//Please note that USB doesn't use Xmodem protocol, since USB already includes flow control and data verification
//Data are simply forwarded without further coding.
const t_monitor_if usbcdc_if =
{
	.put_c =         cdc_putc,
	.get_c =         cdc_getc,
	.is_rx_ready =   cdc_is_rx_ready,
	.putdata =       cdc_write_buf,
	.getdata =       cdc_read_buf,
	.putdata_xmd =   cdc_write_buf,
	.getdata_xmd =   cdc_read_buf_xmd
};
#endif

/* The pointer to the interface object use by the monitor */
t_monitor_if * ptr_monitor_if;

/* b_terminal_mode mode (ascii) or hex mode */
volatile bool b_terminal_mode = false;
volatile bool b_sam_ba_interface_usart = false;

/* Pulse generation counters to keep track of the time remaining for each pulse type */
#define TX_RX_LED_PULSE_PERIOD 100
volatile uint16_t txLEDPulse = 0; // time remaining for Tx LED pulse
volatile uint16_t rxLEDPulse = 0; // time remaining for Rx LED pulse

void sam_ba_monitor_init(uint8_t com_interface)
{
	#if SAM_BA_INTERFACE == SAM_BA_UART_ONLY  ||  SAM_BA_INTERFACE == SAM_BA_BOTH_INTERFACES
	//Selects the requested interface for future actions
	if (com_interface == SAM_BA_INTERFACE_USART)
	{
		ptr_monitor_if = (t_monitor_if*) &uart_if;
		b_sam_ba_interface_usart = true;
	}
	#endif
	#if SAM_BA_INTERFACE == SAM_BA_USBCDC_ONLY  ||  SAM_BA_INTERFACE == SAM_BA_BOTH_INTERFACES
	if (com_interface == SAM_BA_INTERFACE_USBCDC)
	{
		ptr_monitor_if = (t_monitor_if*) &usbcdc_if;
	}
	#endif
}

/*
* Central SAM-BA monitor putdata function using the board LEDs
*/
static uint32_t sam_ba_putdata(t_monitor_if* pInterface, void const* data, uint32_t length)
{
	uint32_t result;
	
	if(usbComOpen){
		result=pInterface->putdata(data, length);
		} else {
		result = 0;
	}

	//LEDTX_on();
	//txLEDPulse = TX_RX_LED_PULSE_PERIOD;

	return result;
}

/*
* Central SAM-BA monitor getdata function using the board LEDs
*/
static uint32_t sam_ba_getdata(t_monitor_if* pInterface, void* data, uint32_t length)
{
	uint32_t result ;

	result=pInterface->getdata(data, length);

	if (result)
	{
		//LEDRX_on();
		//rxLEDPulse = TX_RX_LED_PULSE_PERIOD;
	}

	return result;
}

/*
* Central SAM-BA monitor putdata function using the board LEDs
*/
static uint32_t sam_ba_putdata_xmd(t_monitor_if* pInterface, void const* data, uint32_t length)
{
	uint32_t result ;

	result=pInterface->putdata_xmd(data, length);

	//LEDTX_on();
	//txLEDPulse = TX_RX_LED_PULSE_PERIOD;

	return result;
}

/*
* Central SAM-BA monitor getdata function using the board LEDs
*/
static uint32_t sam_ba_getdata_xmd(t_monitor_if* pInterface, void* data, uint32_t length)
{
	uint32_t result ;

	result=pInterface->getdata_xmd(data, length);

	if (result)
	{
		//LEDRX_on();
		//rxLEDPulse = TX_RX_LED_PULSE_PERIOD;
	}

	return result;
}

/**
* \brief This function allows data emission by USART
*
* \param *data  Data pointer
* \param length Length of the data
*/
void sam_ba_putdata_term(uint8_t* data, uint32_t length)
{
	uint8_t temp, buf[12], *data_ascii;
	uint32_t i, int_value;

	if (b_terminal_mode)
	{
		if (length == 4)
		int_value = *(uint32_t *) data;
		else if (length == 2)
		int_value = *(uint16_t *) data;
		else
		int_value = *(uint8_t *) data;

		data_ascii = buf + 2;
		data_ascii += length * 2 - 1;

		for (i = 0; i < length * 2; i++)
		{
			temp = (uint8_t) (int_value & 0xf);

			if (temp <= 0x9)
			*data_ascii = temp | 0x30;
			else
			*data_ascii = temp + 0x37;

			int_value >>= 4;
			data_ascii--;
		}
		buf[0] = '0';
		buf[1] = 'x';
		buf[length * 2 + 2] = '\n';
		buf[length * 2 + 3] = '\r';
		sam_ba_putdata(ptr_monitor_if, buf, length * 2 + 4);
	}
	else
	sam_ba_putdata(ptr_monitor_if, data, length);
	return;
}

volatile uint32_t sp;
void call_applet(uint32_t address)
{
	uint32_t app_start_address;

	__disable_irq();

	sp = __get_MSP();

	/* Rebase the Stack Pointer */
	__set_MSP(*(uint32_t *) address);

	/* Load the Reset Handler address of the application */
	app_start_address = *(uint32_t *)(address + 4);

	/* Jump to application Reset Handler in the application */
	asm("bx %0"::"r"(app_start_address));
}

uint32_t current_number;
uint32_t i, length;
uint8_t command, *ptr_data, *ptr, data[SIZEBUFMAX];
uint8_t j;
uint32_t u32tmp;

uint32_t PAGE_SIZE, PAGES, MAX_FLASH;

// Prints a 32-bit integer in hex.
static void put_uint32(uint32_t n)
{
	char buff[8];
	int i;
	for (i=0; i<8; i++)
	{
		int d = n & 0XF;
		n = (n >> 4);

		buff[7-i] = d > 9 ? 'A' + d - 10 : '0' + d;
	}
	sam_ba_putdata( ptr_monitor_if, buff, 8);
}

uint32_t tick = 0;
uint8_t data_out[3] = {12, 48, 64}; 

#define APA_CDC_NUM_STATIC_PACKETS 6
uint8_t packCDC[APA_CDC_NUM_STATIC_PACKETS][256]; // static memory buffer for packets
uint8_t packCDC_num = 0;
uint8_t packCDC_position = 0;
uint8_t packCDC_readyNum = 0;

static void sam_ba_monitor_loop(void)
{
	// get bytes, handle.. 
	length = sam_ba_getdata(ptr_monitor_if, data, SIZEBUFMAX);

	// this should be better: but before we send we have to make sure COM is open, else port locked
	if(length > 0 && !usbComOpen){
		usbComOpen = 1;
		pin_clear(&stlUsb);
		// uint8_t hello[2] = {0,0};
		// sam_ba_putdata(ptr_monitor_if, &hello, 2);
		return;
	} 
	
	// chunk packets into packCDC
	if(length > 0 && usbComOpen){
		pin_clear(&stlPacket);
		for(int i = 0; i < length; i ++){
			packCDC[packCDC_num][packCDC_position] = data[i];
			packCDC_position ++;
			if(packCDC_position >= packCDC[packCDC_num][0]){
				packCDC_num = (packCDC_num + 1) % APA_CDC_NUM_STATIC_PACKETS;
				packCDC_readyNum ++;
				packCDC_position = 0;
			}
		}
	}
	
	// handle all of the ready packets
	while(packCDC_readyNum > 0){
		uint8_t p = (packCDC_num + APA_CDC_NUM_STATIC_PACKETS - packCDC_readyNum) % APA_CDC_NUM_STATIC_PACKETS;
		// we're going to forward them all, 1st byte is port out 
		uint8_t port = packCDC[p][1];
		if(port > NUM_UPS){
			port = NUM_UPS - 1; // if overrun, go on last port
		}
		uart_sendchars_buffered(ups[port], packCDC[p], packCDC[p][0]);
		packCDC_readyNum --;
	}
	
	// reply USB direct to debug if USB coming thru
	// sam_ba_putdata(ptr_monitor_if, data, length);
	
	// have to implement APA code here? and watch for multipackets?
	pin_set(&stlPacket);
	
	// dirt nasty heartbeat
	/*
	if(!(tick % 100000)){
		sam_ba_putdata(ptr_monitor_if, &data_out, 3);
	}
	tick ++;
	*/
}

void sam_ba_port_put_data(uint8_t *data, uint32_t length){
	sam_ba_putdata(ptr_monitor_if, data, length);
}

void sam_ba_monitor_sys_tick(void)
{
	/* Check whether the TX or RX LED one-shot period has elapsed.  if so, turn off the LED */
	/*
	if (txLEDPulse && !(--txLEDPulse))
	LEDTX_off();
	if (rxLEDPulse && !(--rxLEDPulse))
	LEDRX_off();
	*/
}

/**
* \brief This function starts the SAM-BA monitor.
*/
void sam_ba_monitor_run(void)
{
	/*
	uint32_t pageSizes[] = { 8, 16, 32, 64, 128, 256, 512, 1024 };
	PAGE_SIZE = pageSizes[NVMCTRL->PARAM.bit.PSZ];
	PAGES = NVMCTRL->PARAM.bit.NVMP;
	MAX_FLASH = PAGE_SIZE * PAGES;

	ptr_data = NULL;
	command = 'z';
	*/

	sam_ba_monitor_loop();
}

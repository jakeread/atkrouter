/*
 * hardware.h
 *
 * Created: 5/18/2018 11:01:12 AM
 *  Author: Jake
 */ 




#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "pin.h"
#include "ringbuffer.h"
#include "uartport.h"
#include "apaport.h"

#include "sam_ba_monitor.h"

// For if-case init

#define HARDWARE_IS_APBA 0
#define HARDWARE_IS_APBB 1
#define HARDWARE_IS_APBC 2
#define HARDWARE_IS_APBD 3

#define HARDWARE_ON_PERIPHERAL_A 0x0
#define HARDWARE_ON_PERIPHERAL_B 0x1
#define HARDWARE_ON_PERIPHERAL_C 0x2
#define HARDWARE_ON_PERIPHERAL_D 0x3

// use BAUD_SYSTEM 63019 for FTDI-Limited 115200 baudrate // ~ 4.2us bit period
// use BAUD_SYSTEM 60502 for 230400
// use BAUD_SYSTEM 55469 for 460800
// use BAUD_SYSTEM 45403 for 921600

#define BAUD_SYSTEM 63019

/* USB PINS

USBDM		PA24	USB
USBDP		PA25	USB
*/

/* STATUS LIGHTS

STLTICKER	PA03
STLUSB		PA02
STLPACKET	PB03
STLERR		PB02
*/

pin_t stlTicker;
pin_t stlUsb;
pin_t stlPacket;
pin_t stlErr;

/*
UP0RX		PA05	SER0-1
UP0TX		PA04	SER0-0
UP0STLB		PB22	(on receive)
UP0STLR		PB31	(on transmit)
*/

ringbuffer_t up0rbrx;
ringbuffer_t up0rbtx;

uartport_t up0;

apaport_t apap0;

pin_t up0stlb; // on receive
pin_t up0stlr; // on transmit


/*
UP1RX		PA01	SER1-1
UP1TX		PA00	SER1-0
UP1STLB		PA20
UP1STLR		PA21
*/

ringbuffer_t up1rbrx;
ringbuffer_t up1rbtx;

uartport_t up1;

apaport_t apap1;

pin_t up1stlb;
pin_t up1stlr;

/*
UP2RX		PA08	SER2-1
UP2TX		PA09	SER2-0
UP2STLB		PB16
UP2STLR		PB17 
*/

ringbuffer_t up2rbrx;
ringbuffer_t up2rbtx;

uartport_t up2;

apaport_t apap2;

pin_t up2stlb;
pin_t up2stlr;

/*
UP3RX		PA16	SER3-1
UP3TX		PA17	SER3-0
UP3STLB		PA18 
UP3STLR		PA19
*/

ringbuffer_t up3rbrx;
ringbuffer_t up3rbtx;

uartport_t up3;

apaport_t apap3;

pin_t up3stlb;
pin_t up3stlr;

/*
UP4RX		PA12	SER4-1
UP4TX		PA13	SER4-0
UP4STLB		PA14 
UP4STLR		PA15 
*/

ringbuffer_t up4rbrx;
ringbuffer_t up4rbtx;

uartport_t up4;

apaport_t apap4;

pin_t up4stlb;
pin_t up4stlr;

/*
UP5RX		PA22	SER5-1
UP5TX		PA23	SER5-0
UP5STLB		PB13
UP5STLR		PB14
*/

ringbuffer_t up5rbrx;
ringbuffer_t up5rbtx;

uartport_t up5;

apaport_t apap5;

pin_t up5stlb;
pin_t up5stlr;

// pointers to uartports
#define NUM_UPS 6
uartport_t *ups[NUM_UPS];

#endif /* HARDWARE_H_ */
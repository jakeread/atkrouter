
#include <sam.h>
#include "board_definitions.h"

void clock_init(void)
{
	
	/* Set 1 Flash Wait State for 48MHz */
	NVMCTRL->CTRLA.reg |= NVMCTRL_CTRLA_RWS(0);

	/* ----------------------------------------------------------------------------------------------
	* 1) Enable XOSC32K clock (External on-board 32.768Hz oscillator)
	*/
	OSC32KCTRL->OSCULP32K.reg = OSC32KCTRL_OSCULP32K_EN32K;
	/*
	while( (OSC32KCTRL->STATUS.reg & OSC32KCTRL_STATUS_XOSC32KRDY) == 0 ){
	// wait ready
	}
	*/
	
	OSC32KCTRL->RTCCTRL.bit.RTCSEL = OSC32KCTRL_RTCCTRL_RTCSEL_ULP1K;


	/* Software reset the module to ensure it is re-initialized correctly */
	/* Note: Due to synchronization, there is a delay from writing CTRL.SWRST until the reset is complete.
	* CTRL.SWRST and STATUS.SYNCBUSY will both be cleared when the reset is complete
	*/
	GCLK->CTRLA.bit.SWRST = 1;
	while ( GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_SWRST ){
		/* wait for reset to complete */
	}
	
	/* ----------------------------------------------------------------------------------------------
	* 2) Put XOSC32K as source of Generic Clock Generator 3
	*/
	GCLK->GENCTRL[3].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_OSCULP32K) | //generic clock gen 3
	GCLK_GENCTRL_GENEN;

	while ( GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL3 ){
		/* Wait for synchronization */
	}
	
	/* ----------------------------------------------------------------------------------------------
	* 3) Put Generic Clock Generator 3 as source for Generic Clock Gen 0 (DFLL48M reference)
	*/
	GCLK->GENCTRL[0].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_OSCULP32K) | GCLK_GENCTRL_GENEN;
	
	/* ----------------------------------------------------------------------------------------------
	* 4) Enable DFLL48M clock
	*/

	/* DFLL Configuration in Open Loop mode */

	OSCCTRL->DFLLCTRLA.reg = 0;
	//GCLK->PCHCTRL[OSCCTRL_GCLK_ID_DFLL48].reg = (1 << GCLK_PCHCTRL_CHEN_Pos) | GCLK_PCHCTRL_GEN(GCLK_PCHCTRL_GEN_GCLK3_Val);

	OSCCTRL->DFLLMUL.reg = OSCCTRL_DFLLMUL_CSTEP( 0x1 ) |
	OSCCTRL_DFLLMUL_FSTEP( 0x1 ) |
	OSCCTRL_DFLLMUL_MUL( 0 );

	while ( OSCCTRL->DFLLSYNC.reg & OSCCTRL_DFLLSYNC_DFLLMUL )
	{
		/* Wait for synchronization */
	}
	
	OSCCTRL->DFLLCTRLB.reg = 0;
	while ( OSCCTRL->DFLLSYNC.reg & OSCCTRL_DFLLSYNC_DFLLCTRLB )
	{
		/* Wait for synchronization */
	}
	
	OSCCTRL->DFLLCTRLA.reg |= OSCCTRL_DFLLCTRLA_ENABLE;
	while ( OSCCTRL->DFLLSYNC.reg & OSCCTRL_DFLLSYNC_ENABLE )
	{
		/* Wait for synchronization */
	}
	
	OSCCTRL->DFLLVAL.reg = OSCCTRL->DFLLVAL.reg;
	while( OSCCTRL->DFLLSYNC.bit.DFLLVAL );
	
	OSCCTRL->DFLLCTRLB.reg = OSCCTRL_DFLLCTRLB_WAITLOCK |
	OSCCTRL_DFLLCTRLB_CCDIS | OSCCTRL_DFLLCTRLB_USBCRM ;
	
	while ( !OSCCTRL->STATUS.bit.DFLLRDY )
	{
		/* Wait for synchronization */
	}

	/* ----------------------------------------------------------------------------------------------
	* 5) Switch Generic Clock Generator 0 to DFLL48M. CPU will run at 48MHz.
	*/
	GCLK->GENCTRL[1].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) |
	GCLK_GENCTRL_IDC |
	GCLK_GENCTRL_OE |
	GCLK_GENCTRL_GENEN;

	while ( GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL0 )
	{
		/* Wait for synchronization */
	}
	
	// now we want a DPLL0 for MCLK
	
	// a reference, from the DFLL, for the DPLL0
	GCLK->GENCTRL[5].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL_Val) | GCLK_GENCTRL_GENEN | GCLK_GENCTRL_DIV(24u);
	while(GCLK->SYNCBUSY.bit.GENCTRL5);
	
	// the DPLL setup
	GCLK->PCHCTRL[OSCCTRL_GCLK_ID_FDPLL0].reg = (1 << GCLK_PCHCTRL_CHEN_Pos) | GCLK_PCHCTRL_GEN(GCLK_PCHCTRL_GEN_GCLK5_Val);
	OSCCTRL->Dpll[0].DPLLRATIO.reg = OSCCTRL_DPLLRATIO_LDRFRAC(0x00) | OSCCTRL_DPLLRATIO_LDR(59);
	while(OSCCTRL->Dpll[0].DPLLSYNCBUSY.bit.DPLLRATIO);
	OSCCTRL->Dpll[0].DPLLCTRLB.reg = OSCCTRL_DPLLCTRLB_REFCLK_GCLK | OSCCTRL_DPLLCTRLB_LBYPASS;
	OSCCTRL->Dpll[0].DPLLCTRLA.reg = OSCCTRL_DPLLCTRLA_ENABLE;
	while(OSCCTRL->Dpll[0].DPLLSTATUS.bit.CLKRDY == 0 || OSCCTRL->Dpll[0].DPLLSTATUS.bit.LOCK == 0);
	// set clock to use dpll0
	
	// this would switch the CPU clock to the DPLL0
	GCLK->GENCTRL[0].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DPLL0) | GCLK_GENCTRL_IDC | GCLK_GENCTRL_GENEN;
	while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL0);
	
	/* Turn on the digital interface clock */
	//MCLK->APBAMASK.reg |= MCLK_APBAMASK_GCLK;

	/*
	* Now that all system clocks are configured, we can set CLKDIV .
	* These values are normally the ones present after Reset.
	*/
	MCLK->CPUDIV.reg = MCLK_CPUDIV_DIV_DIV1;
}
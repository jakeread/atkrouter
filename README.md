# AutomataKit Router

This is a message passing device and USB link for the [automatakit](https://gitlab.cba.mit.edu/jakeread/automatakit) architecture.

![board](/images/atkrouter.jpg)

The board includes one USB-to-UART Bridge, a CP2102n, *which is treated as the router's 6th port*. 

The board uses a ~ 2A 5V Buck Regulator, and can power / listen to a Raspberry Pi via a few pogo pins. 

## Development Notes

See [circuit chatter](/circuit) and [firmware chatter](/embedded).

# Viable Commands

### Test

Keycode: **127**
 - to test networking, this will reply with a payload packet containing ```127, 12, 24, 48``` and will toggle an LED on the board

### Reset

Keycode: **128**
 - issues a software microcontroller reset
 - if the microcontroller is already hung up, this will not work

# Reproducing This Work

All automatakit works are open source, and while we cannot sell you boards, if you have an interesting application, get in touch to ask about collaborating.

To reproduce boards and load code, see the document ['Reproducing Automatakit Work'](https://gitlab.cba.mit.edu/jakeread/automatakit/reproduction)
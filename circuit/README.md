# AutomataKit Router Circuit Design

![schematic](https://gitlab.cba.mit.edu/jakeread/atkrouter/raw/master/images/schematic.png)

![routing](https://gitlab.cba.mit.edu/jakeread/atkrouter/raw/master/images/routed.png)

The circuit uses an ATxmega256A3U microcontroller and it's UART modules to chatter over 5 ports. It includes a CP2102n USB - UART bridge, acting as the 6th port, to receive serial messages from a computer.

# BOM

0.1uF
1uF
10uF

120R
470R
1k

Resonator
LED Y,G,B

PWRPAD
2x3 6PIN
RJ45
POGOPIN
USBCONN

RST BUTTON

XMEGA
CP2102
VREG-AP2112
RS-485-SN75C1168

# Notes

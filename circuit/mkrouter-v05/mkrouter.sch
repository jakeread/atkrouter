<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.0.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="atsam">
<packages>
<package name="TQFP100-14X14">
<description>&lt;b&gt;Thin Quad Flat Pack&lt;/b&gt; 14x14 mm&lt;p&gt;</description>
<wire x1="-6.873" y1="6.873" x2="6.873" y2="6.873" width="0.1524" layer="21"/>
<wire x1="6.873" y1="6.873" x2="6.873" y2="-6.873" width="0.1524" layer="21"/>
<wire x1="6.873" y1="-6.873" x2="-6.123" y2="-6.873" width="0.1524" layer="21"/>
<wire x1="-6.123" y1="-6.873" x2="-6.873" y2="-6.123" width="0.1524" layer="21"/>
<wire x1="-6.873" y1="-6.123" x2="-6.873" y2="6.873" width="0.1524" layer="21"/>
<circle x="-4.5" y="-4.5" radius="1" width="0.1524" layer="21"/>
<smd name="75" x="-6" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="74" x="-5.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="73" x="-5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="72" x="-4.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="71" x="-4" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="70" x="-3.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="69" x="-3" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="68" x="-2.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="67" x="-2" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="66" x="-1.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="65" x="-1" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="64" x="-0.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="63" x="0" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="62" x="0.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="61" x="1" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="60" x="1.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="59" x="2" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="58" x="2.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="57" x="3" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="56" x="3.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="55" x="4" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="54" x="4.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="53" x="5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="52" x="5.5" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="51" x="6" y="7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="1" x="-6" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="2" x="-5.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="3" x="-5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="4" x="-4.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="5" x="-4" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="6" x="-3.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="7" x="-3" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="8" x="-2.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="9" x="-2" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="10" x="-1.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="11" x="-1" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="12" x="-0.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="13" x="0" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="14" x="0.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="15" x="1" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="16" x="1.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="17" x="2" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="18" x="2.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="19" x="3" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="20" x="3.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="21" x="4" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="22" x="4.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="23" x="5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="24" x="5.5" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="25" x="6" y="-7.75" dx="0.27" dy="1.5" layer="1"/>
<smd name="26" x="7.75" y="-6" dx="1.5" dy="0.27" layer="1"/>
<smd name="27" x="7.75" y="-5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="28" x="7.75" y="-5" dx="1.5" dy="0.27" layer="1"/>
<smd name="29" x="7.75" y="-4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="30" x="7.75" y="-4" dx="1.5" dy="0.27" layer="1"/>
<smd name="31" x="7.75" y="-3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="32" x="7.75" y="-3" dx="1.5" dy="0.27" layer="1"/>
<smd name="33" x="7.75" y="-2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="34" x="7.75" y="-2" dx="1.5" dy="0.27" layer="1"/>
<smd name="35" x="7.75" y="-1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="36" x="7.75" y="-1" dx="1.5" dy="0.27" layer="1"/>
<smd name="37" x="7.75" y="-0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="38" x="7.75" y="0" dx="1.5" dy="0.27" layer="1"/>
<smd name="39" x="7.75" y="0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="40" x="7.75" y="1" dx="1.5" dy="0.27" layer="1"/>
<smd name="41" x="7.75" y="1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="42" x="7.75" y="2" dx="1.5" dy="0.27" layer="1"/>
<smd name="43" x="7.75" y="2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="44" x="7.75" y="3" dx="1.5" dy="0.27" layer="1"/>
<smd name="45" x="7.75" y="3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="46" x="7.75" y="4" dx="1.5" dy="0.27" layer="1"/>
<smd name="47" x="7.75" y="4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="48" x="7.75" y="5" dx="1.5" dy="0.27" layer="1"/>
<smd name="49" x="7.75" y="5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="50" x="7.75" y="6" dx="1.5" dy="0.27" layer="1"/>
<smd name="76" x="-7.75" y="6" dx="1.5" dy="0.27" layer="1"/>
<smd name="77" x="-7.75" y="5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="78" x="-7.75" y="5" dx="1.5" dy="0.27" layer="1"/>
<smd name="79" x="-7.75" y="4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="80" x="-7.75" y="4" dx="1.5" dy="0.27" layer="1"/>
<smd name="81" x="-7.75" y="3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="82" x="-7.75" y="3" dx="1.5" dy="0.27" layer="1"/>
<smd name="83" x="-7.75" y="2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="84" x="-7.75" y="2" dx="1.5" dy="0.27" layer="1"/>
<smd name="85" x="-7.75" y="1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="86" x="-7.75" y="1" dx="1.5" dy="0.27" layer="1"/>
<smd name="87" x="-7.75" y="0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="88" x="-7.75" y="0" dx="1.5" dy="0.27" layer="1"/>
<smd name="89" x="-7.75" y="-0.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="90" x="-7.75" y="-1" dx="1.5" dy="0.27" layer="1"/>
<smd name="91" x="-7.75" y="-1.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="92" x="-7.75" y="-2" dx="1.5" dy="0.27" layer="1"/>
<smd name="93" x="-7.75" y="-2.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="94" x="-7.75" y="-3" dx="1.5" dy="0.27" layer="1"/>
<smd name="95" x="-7.75" y="-3.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="96" x="-7.75" y="-4" dx="1.5" dy="0.27" layer="1"/>
<smd name="97" x="-7.75" y="-4.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="98" x="-7.75" y="-5" dx="1.5" dy="0.27" layer="1"/>
<smd name="99" x="-7.75" y="-5.5" dx="1.5" dy="0.27" layer="1"/>
<smd name="100" x="-7.75" y="-6" dx="1.5" dy="0.27" layer="1"/>
<text x="-6.223" y="8.763" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.135" y1="6.868" x2="-5.865" y2="7.873" layer="51"/>
<rectangle x1="-5.635" y1="6.868" x2="-5.365" y2="7.873" layer="51"/>
<rectangle x1="-5.135" y1="6.868" x2="-4.865" y2="7.873" layer="51"/>
<rectangle x1="-4.635" y1="6.868" x2="-4.365" y2="7.873" layer="51"/>
<rectangle x1="-4.135" y1="6.868" x2="-3.865" y2="7.873" layer="51"/>
<rectangle x1="-3.635" y1="6.868" x2="-3.365" y2="7.873" layer="51"/>
<rectangle x1="-3.135" y1="6.868" x2="-2.865" y2="7.873" layer="51"/>
<rectangle x1="-2.635" y1="6.868" x2="-2.365" y2="7.873" layer="51"/>
<rectangle x1="-2.135" y1="6.868" x2="-1.865" y2="7.873" layer="51"/>
<rectangle x1="-1.635" y1="6.868" x2="-1.365" y2="7.873" layer="51"/>
<rectangle x1="-1.135" y1="6.868" x2="-0.865" y2="7.873" layer="51"/>
<rectangle x1="-0.635" y1="6.868" x2="-0.365" y2="7.873" layer="51"/>
<rectangle x1="-0.135" y1="6.868" x2="0.135" y2="7.873" layer="51"/>
<rectangle x1="0.365" y1="6.868" x2="0.635" y2="7.873" layer="51"/>
<rectangle x1="0.865" y1="6.868" x2="1.135" y2="7.873" layer="51"/>
<rectangle x1="1.365" y1="6.868" x2="1.635" y2="7.873" layer="51"/>
<rectangle x1="1.865" y1="6.868" x2="2.135" y2="7.873" layer="51"/>
<rectangle x1="2.365" y1="6.868" x2="2.635" y2="7.873" layer="51"/>
<rectangle x1="2.865" y1="6.868" x2="3.135" y2="7.873" layer="51"/>
<rectangle x1="3.365" y1="6.868" x2="3.635" y2="7.873" layer="51"/>
<rectangle x1="3.865" y1="6.868" x2="4.135" y2="7.873" layer="51"/>
<rectangle x1="4.365" y1="6.868" x2="4.635" y2="7.873" layer="51"/>
<rectangle x1="4.865" y1="6.868" x2="5.135" y2="7.873" layer="51"/>
<rectangle x1="5.365" y1="6.868" x2="5.635" y2="7.873" layer="51"/>
<rectangle x1="5.865" y1="6.868" x2="6.135" y2="7.873" layer="51"/>
<rectangle x1="6.873" y1="5.865" x2="7.878" y2="6.135" layer="51"/>
<rectangle x1="6.873" y1="5.365" x2="7.878" y2="5.635" layer="51"/>
<rectangle x1="6.873" y1="4.865" x2="7.878" y2="5.135" layer="51"/>
<rectangle x1="6.873" y1="4.365" x2="7.878" y2="4.635" layer="51"/>
<rectangle x1="6.873" y1="3.865" x2="7.878" y2="4.135" layer="51"/>
<rectangle x1="6.873" y1="3.365" x2="7.878" y2="3.635" layer="51"/>
<rectangle x1="6.873" y1="2.865" x2="7.878" y2="3.135" layer="51"/>
<rectangle x1="6.873" y1="2.365" x2="7.878" y2="2.635" layer="51"/>
<rectangle x1="6.873" y1="1.865" x2="7.878" y2="2.135" layer="51"/>
<rectangle x1="6.873" y1="1.365" x2="7.878" y2="1.635" layer="51"/>
<rectangle x1="6.873" y1="0.865" x2="7.878" y2="1.135" layer="51"/>
<rectangle x1="6.873" y1="0.365" x2="7.878" y2="0.635" layer="51"/>
<rectangle x1="6.873" y1="-0.135" x2="7.878" y2="0.135" layer="51"/>
<rectangle x1="6.873" y1="-0.635" x2="7.878" y2="-0.365" layer="51"/>
<rectangle x1="6.873" y1="-1.135" x2="7.878" y2="-0.865" layer="51"/>
<rectangle x1="6.873" y1="-1.635" x2="7.878" y2="-1.365" layer="51"/>
<rectangle x1="6.873" y1="-2.135" x2="7.878" y2="-1.865" layer="51"/>
<rectangle x1="6.873" y1="-2.635" x2="7.878" y2="-2.365" layer="51"/>
<rectangle x1="6.873" y1="-3.135" x2="7.878" y2="-2.865" layer="51"/>
<rectangle x1="6.873" y1="-3.635" x2="7.878" y2="-3.365" layer="51"/>
<rectangle x1="6.873" y1="-4.135" x2="7.878" y2="-3.865" layer="51"/>
<rectangle x1="6.873" y1="-4.635" x2="7.878" y2="-4.365" layer="51"/>
<rectangle x1="6.873" y1="-5.135" x2="7.878" y2="-4.865" layer="51"/>
<rectangle x1="6.873" y1="-5.635" x2="7.878" y2="-5.365" layer="51"/>
<rectangle x1="6.873" y1="-6.135" x2="7.878" y2="-5.865" layer="51"/>
<rectangle x1="5.865" y1="-7.873" x2="6.135" y2="-6.868" layer="51"/>
<rectangle x1="5.365" y1="-7.873" x2="5.635" y2="-6.868" layer="51"/>
<rectangle x1="4.865" y1="-7.873" x2="5.135" y2="-6.868" layer="51"/>
<rectangle x1="4.365" y1="-7.873" x2="4.635" y2="-6.868" layer="51"/>
<rectangle x1="3.865" y1="-7.873" x2="4.135" y2="-6.868" layer="51"/>
<rectangle x1="3.365" y1="-7.873" x2="3.635" y2="-6.868" layer="51"/>
<rectangle x1="2.865" y1="-7.873" x2="3.135" y2="-6.868" layer="51"/>
<rectangle x1="2.365" y1="-7.873" x2="2.635" y2="-6.868" layer="51"/>
<rectangle x1="1.865" y1="-7.873" x2="2.135" y2="-6.868" layer="51"/>
<rectangle x1="1.365" y1="-7.873" x2="1.635" y2="-6.868" layer="51"/>
<rectangle x1="0.865" y1="-7.873" x2="1.135" y2="-6.868" layer="51"/>
<rectangle x1="0.365" y1="-7.873" x2="0.635" y2="-6.868" layer="51"/>
<rectangle x1="-0.135" y1="-7.873" x2="0.135" y2="-6.868" layer="51"/>
<rectangle x1="-0.635" y1="-7.873" x2="-0.365" y2="-6.868" layer="51"/>
<rectangle x1="-1.135" y1="-7.873" x2="-0.865" y2="-6.868" layer="51"/>
<rectangle x1="-1.635" y1="-7.873" x2="-1.365" y2="-6.868" layer="51"/>
<rectangle x1="-2.135" y1="-7.873" x2="-1.865" y2="-6.868" layer="51"/>
<rectangle x1="-2.635" y1="-7.873" x2="-2.365" y2="-6.868" layer="51"/>
<rectangle x1="-3.135" y1="-7.873" x2="-2.865" y2="-6.868" layer="51"/>
<rectangle x1="-3.635" y1="-7.873" x2="-3.365" y2="-6.868" layer="51"/>
<rectangle x1="-4.135" y1="-7.873" x2="-3.865" y2="-6.868" layer="51"/>
<rectangle x1="-4.635" y1="-7.873" x2="-4.365" y2="-6.868" layer="51"/>
<rectangle x1="-5.135" y1="-7.873" x2="-4.865" y2="-6.868" layer="51"/>
<rectangle x1="-5.635" y1="-7.873" x2="-5.365" y2="-6.868" layer="51"/>
<rectangle x1="-6.135" y1="-7.873" x2="-5.865" y2="-6.868" layer="51"/>
<rectangle x1="-7.878" y1="-6.135" x2="-6.873" y2="-5.865" layer="51"/>
<rectangle x1="-7.878" y1="-5.635" x2="-6.873" y2="-5.365" layer="51"/>
<rectangle x1="-7.878" y1="-5.135" x2="-6.873" y2="-4.865" layer="51"/>
<rectangle x1="-7.878" y1="-4.635" x2="-6.873" y2="-4.365" layer="51"/>
<rectangle x1="-7.878" y1="-4.135" x2="-6.873" y2="-3.865" layer="51"/>
<rectangle x1="-7.878" y1="-3.635" x2="-6.873" y2="-3.365" layer="51"/>
<rectangle x1="-7.878" y1="-3.135" x2="-6.873" y2="-2.865" layer="51"/>
<rectangle x1="-7.878" y1="-2.635" x2="-6.873" y2="-2.365" layer="51"/>
<rectangle x1="-7.878" y1="-2.135" x2="-6.873" y2="-1.865" layer="51"/>
<rectangle x1="-7.878" y1="-1.635" x2="-6.873" y2="-1.365" layer="51"/>
<rectangle x1="-7.878" y1="-1.135" x2="-6.873" y2="-0.865" layer="51"/>
<rectangle x1="-7.878" y1="-0.635" x2="-6.873" y2="-0.365" layer="51"/>
<rectangle x1="-7.878" y1="-0.135" x2="-6.873" y2="0.135" layer="51"/>
<rectangle x1="-7.878" y1="0.365" x2="-6.873" y2="0.635" layer="51"/>
<rectangle x1="-7.878" y1="0.865" x2="-6.873" y2="1.135" layer="51"/>
<rectangle x1="-7.878" y1="1.365" x2="-6.873" y2="1.635" layer="51"/>
<rectangle x1="-7.878" y1="1.865" x2="-6.873" y2="2.135" layer="51"/>
<rectangle x1="-7.878" y1="2.365" x2="-6.873" y2="2.635" layer="51"/>
<rectangle x1="-7.878" y1="2.865" x2="-6.873" y2="3.135" layer="51"/>
<rectangle x1="-7.878" y1="3.365" x2="-6.873" y2="3.635" layer="51"/>
<rectangle x1="-7.878" y1="3.865" x2="-6.873" y2="4.135" layer="51"/>
<rectangle x1="-7.878" y1="4.365" x2="-6.873" y2="4.635" layer="51"/>
<rectangle x1="-7.878" y1="4.865" x2="-6.873" y2="5.135" layer="51"/>
<rectangle x1="-7.878" y1="5.365" x2="-6.873" y2="5.635" layer="51"/>
<rectangle x1="-7.878" y1="5.865" x2="-6.873" y2="6.135" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ATSAMS70N">
<pin name="PA0/I2SC0_MCK" x="30.48" y="78.74" length="middle" rot="R180"/>
<pin name="PA1/I2SC0_CK" x="30.48" y="76.2" length="middle" rot="R180"/>
<pin name="PA2" x="30.48" y="73.66" length="middle" rot="R180"/>
<pin name="PA3/TWD0" x="30.48" y="71.12" length="middle" rot="R180"/>
<pin name="PA4/UTXD1/TWCK0" x="30.48" y="68.58" length="middle" rot="R180"/>
<pin name="PA5/URXD1" x="30.48" y="66.04" length="middle" rot="R180"/>
<pin name="PA7/XIN32" x="30.48" y="63.5" length="middle" rot="R180"/>
<pin name="PA8/XOUT32" x="30.48" y="60.96" length="middle" rot="R180"/>
<pin name="PA9/URXD0" x="30.48" y="58.42" length="middle" rot="R180"/>
<pin name="PA10/UTXD0" x="30.48" y="55.88" length="middle" rot="R180"/>
<pin name="PA11/PWMC0_PWMH0" x="30.48" y="53.34" length="middle" rot="R180"/>
<pin name="PA12/PWMC0_PWMH1" x="30.48" y="50.8" length="middle" rot="R180"/>
<pin name="PA13/PWMC0_PWMH2" x="30.48" y="48.26" length="middle" rot="R180"/>
<pin name="PA14/WMC0_PWMH3" x="30.48" y="45.72" length="middle" rot="R180"/>
<pin name="PA15/PWMC0_PWML3" x="30.48" y="43.18" length="middle" rot="R180"/>
<pin name="PA16/PWMC0_PWML2" x="30.48" y="40.64" length="middle" rot="R180"/>
<pin name="PA17/AFE0_AD6" x="30.48" y="38.1" length="middle" rot="R180"/>
<pin name="PA18/AFE0_AD7" x="30.48" y="35.56" length="middle" rot="R180"/>
<pin name="PA19/PWMC0_PWML0/AFE0_AD8" x="30.48" y="33.02" length="middle" rot="R180"/>
<pin name="PA20/PWMC0_PWML1/AFE0_AD9" x="30.48" y="30.48" length="middle" rot="R180"/>
<pin name="PA21/AFE0_AD1" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="PA22" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="PA23" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="PA24" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="PA25" x="30.48" y="17.78" length="middle" rot="R180"/>
<pin name="PA26" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="PA27" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="PA28" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="PA30" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="PA31/SPI0_NPCS1" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="PB0/RXD0" x="-33.02" y="-76.2" length="middle"/>
<pin name="PB1/TXD0" x="-33.02" y="-73.66" length="middle"/>
<pin name="PB2/SPI0_NPCS0/CTS0" x="-33.02" y="-71.12" length="middle"/>
<pin name="PB3/RTS0" x="-33.02" y="-68.58" length="middle"/>
<pin name="PB4/TDI/TWD1" x="-33.02" y="-66.04" length="middle"/>
<pin name="PB5/TDO/TRACESWO/TWCK1" x="-33.02" y="-63.5" length="middle"/>
<pin name="PB6/SWDIO/TMS" x="-33.02" y="-60.96" length="middle"/>
<pin name="PB7/SWCLK/TCK" x="-33.02" y="-58.42" length="middle"/>
<pin name="PB8/XOUT" x="-33.02" y="-55.88" length="middle"/>
<pin name="PB9/XIN" x="-33.02" y="-53.34" length="middle"/>
<pin name="PB12/ERASE" x="-33.02" y="-50.8" length="middle"/>
<pin name="PB13/DAC0/SCK0" x="-33.02" y="-48.26" length="middle"/>
<pin name="PD1/PWMC1_PWMH0" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="PD2/PWMC1_PWML1" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="PD3/PWMC1_PWMH1" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="PD4/PWMC1_PWML2" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="PD5/PWMC1_PWMH2" x="30.48" y="-12.7" length="middle" rot="R180"/>
<pin name="PD6/PWMC1_PWML3" x="30.48" y="-15.24" length="middle" rot="R180"/>
<pin name="PD7/PWMC1_PWMH3" x="30.48" y="-17.78" length="middle" rot="R180"/>
<pin name="PD8" x="30.48" y="-20.32" length="middle" rot="R180"/>
<pin name="PD9" x="30.48" y="-22.86" length="middle" rot="R180"/>
<pin name="PD10/PWMC0_PWML0" x="30.48" y="-25.4" length="middle" rot="R180"/>
<pin name="PD11/PWMC0_PWMH0" x="30.48" y="-27.94" length="middle" rot="R180"/>
<pin name="PD12/SPI0_NPCS2" x="30.48" y="-30.48" length="middle" rot="R180"/>
<pin name="PD13" x="30.48" y="-33.02" length="middle" rot="R180"/>
<pin name="PD14" x="30.48" y="-35.56" length="middle" rot="R180"/>
<pin name="PD15" x="30.48" y="-38.1" length="middle" rot="R180"/>
<pin name="PD16" x="30.48" y="-40.64" length="middle" rot="R180"/>
<pin name="PD17" x="30.48" y="-43.18" length="middle" rot="R180"/>
<pin name="PD18/URXD4" x="30.48" y="-45.72" length="middle" rot="R180"/>
<pin name="PD19/UTXD4" x="30.48" y="-48.26" length="middle" rot="R180"/>
<pin name="PD20/SPI0_MISO" x="30.48" y="-50.8" length="middle" rot="R180"/>
<pin name="PD21/SPI0_MOSI" x="30.48" y="-53.34" length="middle" rot="R180"/>
<pin name="PD22_SPI0_SPCK" x="30.48" y="-55.88" length="middle" rot="R180"/>
<pin name="PD24" x="30.48" y="-58.42" length="middle" rot="R180"/>
<pin name="PD25/URXD2" x="30.48" y="-60.96" length="middle" rot="R180"/>
<pin name="PD26/UTXD2" x="30.48" y="-63.5" length="middle" rot="R180"/>
<pin name="PD27/SPI0_NPCS3/TWD2" x="30.48" y="-66.04" length="middle" rot="R180"/>
<pin name="PD28/URXD3/TWCK2" x="30.48" y="-68.58" length="middle" rot="R180"/>
<pin name="PD30/UTXD3" x="30.48" y="-71.12" length="middle" rot="R180"/>
<pin name="PD31" x="30.48" y="-73.66" length="middle" rot="R180"/>
<pin name="PD0/DAC1/PWMC1_PWML0" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="VDDIO1" x="-33.02" y="58.42" length="middle"/>
<pin name="VDDIO2" x="-33.02" y="55.88" length="middle"/>
<pin name="VDDIO3" x="-33.02" y="53.34" length="middle"/>
<pin name="VDDIO4" x="-33.02" y="50.8" length="middle"/>
<pin name="JTAGSEL" x="-33.02" y="-38.1" length="middle"/>
<pin name="TST" x="-33.02" y="-40.64" length="middle"/>
<pin name="VREFP" x="-33.02" y="78.74" length="middle"/>
<pin name="NRST" x="-33.02" y="-43.18" length="middle"/>
<pin name="VDDOUT" x="-33.02" y="43.18" length="middle"/>
<pin name="VDDIN" x="-33.02" y="45.72" length="middle"/>
<pin name="VREFN" x="-33.02" y="76.2" length="middle"/>
<pin name="USB_DP" x="-33.02" y="-30.48" length="middle"/>
<pin name="USB_DM" x="-33.02" y="-33.02" length="middle"/>
<pin name="VDDPLL" x="-33.02" y="25.4" length="middle"/>
<pin name="VDDUTMII" x="-33.02" y="71.12" length="middle"/>
<pin name="VDDUTMIC" x="-33.02" y="22.86" length="middle"/>
<pin name="VBG" x="-33.02" y="10.16" length="middle"/>
<pin name="GND6" x="-33.02" y="-10.16" length="middle"/>
<pin name="GND5" x="-33.02" y="-7.62" length="middle"/>
<pin name="VDDCORE1" x="-33.02" y="38.1" length="middle"/>
<pin name="VDDCORE2" x="-33.02" y="35.56" length="middle"/>
<pin name="VDDCORE3" x="-33.02" y="33.02" length="middle"/>
<pin name="VDDCORE4" x="-33.02" y="30.48" length="middle"/>
<pin name="GND1" x="-33.02" y="2.54" length="middle"/>
<pin name="GND2" x="-33.02" y="0" length="middle"/>
<pin name="GND3" x="-33.02" y="-2.54" length="middle"/>
<pin name="GND4" x="-33.02" y="-5.08" length="middle"/>
<pin name="VDDPLLUSB" x="-33.02" y="66.04" length="middle"/>
<wire x1="-27.94" y1="83.82" x2="-27.94" y2="-81.28" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-81.28" x2="25.4" y2="-81.28" width="0.254" layer="94"/>
<wire x1="25.4" y1="-81.28" x2="25.4" y2="83.82" width="0.254" layer="94"/>
<wire x1="25.4" y1="83.82" x2="-27.94" y2="83.82" width="0.254" layer="94"/>
<text x="-2.54" y="86.36" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-83.82" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMS70N-TQFP100" prefix="U">
<gates>
<gate name="G$1" symbol="ATSAMS70N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP100-14X14">
<connects>
<connect gate="G$1" pin="GND1" pad="3"/>
<connect gate="G$1" pin="GND2" pad="7"/>
<connect gate="G$1" pin="GND3" pad="8"/>
<connect gate="G$1" pin="GND4" pad="10"/>
<connect gate="G$1" pin="GND5" pad="29"/>
<connect gate="G$1" pin="GND6" pad="67"/>
<connect gate="G$1" pin="JTAGSEL" pad="73"/>
<connect gate="G$1" pin="NRST" pad="58"/>
<connect gate="G$1" pin="PA0/I2SC0_MCK" pad="72"/>
<connect gate="G$1" pin="PA1/I2SC0_CK" pad="70"/>
<connect gate="G$1" pin="PA10/UTXD0" pad="46"/>
<connect gate="G$1" pin="PA11/PWMC0_PWMH0" pad="44"/>
<connect gate="G$1" pin="PA12/PWMC0_PWMH1" pad="48"/>
<connect gate="G$1" pin="PA13/PWMC0_PWMH2" pad="27"/>
<connect gate="G$1" pin="PA14/WMC0_PWMH3" pad="34"/>
<connect gate="G$1" pin="PA15/PWMC0_PWML3" pad="33"/>
<connect gate="G$1" pin="PA16/PWMC0_PWML2" pad="30"/>
<connect gate="G$1" pin="PA17/AFE0_AD6" pad="16"/>
<connect gate="G$1" pin="PA18/AFE0_AD7" pad="15"/>
<connect gate="G$1" pin="PA19/PWMC0_PWML0/AFE0_AD8" pad="14"/>
<connect gate="G$1" pin="PA2" pad="66"/>
<connect gate="G$1" pin="PA20/PWMC0_PWML1/AFE0_AD9" pad="13"/>
<connect gate="G$1" pin="PA21/AFE0_AD1" pad="21"/>
<connect gate="G$1" pin="PA22" pad="26"/>
<connect gate="G$1" pin="PA23" pad="31"/>
<connect gate="G$1" pin="PA24" pad="38"/>
<connect gate="G$1" pin="PA25" pad="40"/>
<connect gate="G$1" pin="PA26" pad="42"/>
<connect gate="G$1" pin="PA27" pad="50"/>
<connect gate="G$1" pin="PA28" pad="79"/>
<connect gate="G$1" pin="PA3/TWD0" pad="64"/>
<connect gate="G$1" pin="PA30" pad="82"/>
<connect gate="G$1" pin="PA31/SPI0_NPCS1" pad="83"/>
<connect gate="G$1" pin="PA4/UTXD1/TWCK0" pad="55"/>
<connect gate="G$1" pin="PA5/URXD1" pad="52"/>
<connect gate="G$1" pin="PA7/XIN32" pad="24"/>
<connect gate="G$1" pin="PA8/XOUT32" pad="25"/>
<connect gate="G$1" pin="PA9/URXD0" pad="54"/>
<connect gate="G$1" pin="PB0/RXD0" pad="12"/>
<connect gate="G$1" pin="PB1/TXD0" pad="11"/>
<connect gate="G$1" pin="PB12/ERASE" pad="61"/>
<connect gate="G$1" pin="PB13/DAC0/SCK0" pad="100"/>
<connect gate="G$1" pin="PB2/SPI0_NPCS0/CTS0" pad="17"/>
<connect gate="G$1" pin="PB3/RTS0" pad="20"/>
<connect gate="G$1" pin="PB4/TDI/TWD1" pad="74"/>
<connect gate="G$1" pin="PB5/TDO/TRACESWO/TWCK1" pad="77"/>
<connect gate="G$1" pin="PB6/SWDIO/TMS" pad="57"/>
<connect gate="G$1" pin="PB7/SWCLK/TCK" pad="63"/>
<connect gate="G$1" pin="PB8/XOUT" pad="98"/>
<connect gate="G$1" pin="PB9/XIN" pad="99"/>
<connect gate="G$1" pin="PD0/DAC1/PWMC1_PWML0" pad="1"/>
<connect gate="G$1" pin="PD1/PWMC1_PWMH0" pad="92"/>
<connect gate="G$1" pin="PD10/PWMC0_PWML0" pad="71"/>
<connect gate="G$1" pin="PD11/PWMC0_PWMH0" pad="69"/>
<connect gate="G$1" pin="PD12/SPI0_NPCS2" pad="65"/>
<connect gate="G$1" pin="PD13" pad="62"/>
<connect gate="G$1" pin="PD14" pad="59"/>
<connect gate="G$1" pin="PD15" pad="75"/>
<connect gate="G$1" pin="PD16" pad="56"/>
<connect gate="G$1" pin="PD17" pad="53"/>
<connect gate="G$1" pin="PD18/URXD4" pad="49"/>
<connect gate="G$1" pin="PD19/UTXD4" pad="47"/>
<connect gate="G$1" pin="PD2/PWMC1_PWML1" pad="91"/>
<connect gate="G$1" pin="PD20/SPI0_MISO" pad="45"/>
<connect gate="G$1" pin="PD21/SPI0_MOSI" pad="43"/>
<connect gate="G$1" pin="PD22_SPI0_SPCK" pad="41"/>
<connect gate="G$1" pin="PD24" pad="37"/>
<connect gate="G$1" pin="PD25/URXD2" pad="35"/>
<connect gate="G$1" pin="PD26/UTXD2" pad="36"/>
<connect gate="G$1" pin="PD27/SPI0_NPCS3/TWD2" pad="32"/>
<connect gate="G$1" pin="PD28/URXD3/TWCK2" pad="51"/>
<connect gate="G$1" pin="PD3/PWMC1_PWMH1" pad="89"/>
<connect gate="G$1" pin="PD30/UTXD3" pad="23"/>
<connect gate="G$1" pin="PD31" pad="2"/>
<connect gate="G$1" pin="PD4/PWMC1_PWML2" pad="88"/>
<connect gate="G$1" pin="PD5/PWMC1_PWMH2" pad="87"/>
<connect gate="G$1" pin="PD6/PWMC1_PWML3" pad="85"/>
<connect gate="G$1" pin="PD7/PWMC1_PWMH3" pad="84"/>
<connect gate="G$1" pin="PD8" pad="80"/>
<connect gate="G$1" pin="PD9" pad="78"/>
<connect gate="G$1" pin="TST" pad="60"/>
<connect gate="G$1" pin="USB_DM" pad="94"/>
<connect gate="G$1" pin="USB_DP" pad="95"/>
<connect gate="G$1" pin="VBG" pad="97"/>
<connect gate="G$1" pin="VDDCORE1" pad="18"/>
<connect gate="G$1" pin="VDDCORE2" pad="22"/>
<connect gate="G$1" pin="VDDCORE3" pad="39"/>
<connect gate="G$1" pin="VDDCORE4" pad="76"/>
<connect gate="G$1" pin="VDDIN" pad="5"/>
<connect gate="G$1" pin="VDDIO1" pad="19"/>
<connect gate="G$1" pin="VDDIO2" pad="28"/>
<connect gate="G$1" pin="VDDIO3" pad="68"/>
<connect gate="G$1" pin="VDDIO4" pad="81"/>
<connect gate="G$1" pin="VDDOUT" pad="4"/>
<connect gate="G$1" pin="VDDPLL" pad="86"/>
<connect gate="G$1" pin="VDDPLLUSB" pad="90"/>
<connect gate="G$1" pin="VDDUTMIC" pad="96"/>
<connect gate="G$1" pin="VDDUTMII" pad="93"/>
<connect gate="G$1" pin="VREFN" pad="6"/>
<connect gate="G$1" pin="VREFP" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$5" x="0" y="1.5" dx="1.7" dy="0.55" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="DX4R005HJ5_100">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="DX4R005HJ5">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="25" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="DX4R005HJ5_64">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="AYZ0102AGRLC">
<wire x1="-3.6" y1="-1.5" x2="3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="-1.5" x2="3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.6" y1="1.5" x2="-3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="0" y2="2.5" width="0.127" layer="21"/>
<wire x1="0" y1="2.5" x2="1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<smd name="2" x="0" y="-2.3" dx="1.27" dy="1.397" layer="1"/>
<smd name="3" x="-2.5" y="-2.3" dx="1.27" dy="1.397" layer="1"/>
<smd name="1" x="2.5" y="-2.3" dx="1.27" dy="1.397" layer="1"/>
<text x="-2.794" y="2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.302" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-1.5" y="0.4" drill="0.85"/>
<hole x="1.5" y="0.4" drill="0.85"/>
</package>
</packages>
<symbols>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="USB-1">
<wire x1="6.35" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-1.27" size="2.54" layer="94">USB</text>
<text x="-4.445" y="-1.905" size="1.27" layer="95" font="vector" rot="R90">&gt;Name</text>
<text x="8.255" y="-1.905" size="1.27" layer="96" font="vector" rot="R90">&gt;Value</text>
<pin name="D+" x="5.08" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D-" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="VBUS" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="-2.54" y="5.08" visible="pad" length="short" rot="R270"/>
</symbol>
<symbol name="SWITCH-SPDT">
<wire x1="-2.54" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<circle x="2.54" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="-4.064" y="4.318" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.826" y="-6.096" size="1.778" layer="95">&gt;VALUE</text>
<pin name="1" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO-USB" prefix="X">
<description>SMD micro USB connector as found in the fablab inventory. 
Three footprint variants included: 
&lt;ol&gt;
&lt;li&gt; original, as described by manufacturer's datasheet
&lt;li&gt; for milling with the 1/100" bit
&lt;li&gt; for milling with the 1/64" bit
&lt;/ol&gt;
&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="_1/100" package="DX4R005HJ5_100">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ORIG" package="DX4R005HJ5">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1/64" package="DX4R005HJ5_64">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SLIDE-SWITCH" prefix="S">
<description>SMD slide-switch AYZ0102AGRLC as found in the fablab inventory.  Includes the mounting holes.

&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="SWITCH-SPDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AYZ0102AGRLC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X02">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="3.15" x2="5.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_BIG">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.15"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD-VERT">
<description>&lt;h3&gt;JST-Vertical Male Header SMT &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="-3.81" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-3.81" y="2.21" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
</package>
<package name="SCREWTERMINAL-5MM-2">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<description>&lt;h3&gt;Plated Through Hole - Locking Footprint&lt;/h3&gt;
Holes are staggered by 0.005" from center to hold pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2_LOCK">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole Locking Footprint&lt;/h3&gt;
Holes are offset from center by 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads with Locking Footprint&lt;/h3&gt;
Pins are staggered by 0.005" from center to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH Locking&lt;/h3&gt;
Holes are offset from center 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads without Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="2.73" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<description>&lt;h3&gt;Plated Through Hole - 0.1" holes&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.2"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
<text x="-5.08" y="2.667" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins Connector - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole- No Silk&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; No silk outline of connector. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole - KIT&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad.
&lt;br&gt; This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<description>&lt;h3&gt;Spring Terminal- PCB Mount 2 Pin PTH&lt;/h3&gt;
tDocu marks the spring arms
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/SpringTerminal.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<description>&lt;h3&gt;2 Pin Screw Terminal - 2.54mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<text x="-1.27" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_POKEHOME">
<description>2 pin poke-home connector

part number 2062-2P from STA</description>
<wire x1="-7" y1="-4" x2="-7" y2="2" width="0.2032" layer="21"/>
<wire x1="-7" y1="2" x2="-7" y2="4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="4" x2="4.7" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="-4" x2="-7" y2="-4" width="0.2032" layer="21"/>
<smd name="P2" x="5.25" y="-2" dx="3.5" dy="2" layer="1"/>
<smd name="P1" x="5.25" y="2" dx="3.5" dy="2" layer="1"/>
<smd name="P4" x="-4" y="-2" dx="6" dy="2" layer="1"/>
<smd name="P3" x="-4" y="2" dx="6" dy="2" layer="1"/>
<wire x1="-7" y1="4" x2="4.7" y2="4" width="0.2032" layer="21"/>
<text x="0.635" y="-3.175" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_RA_PTH_FEMALE">
<wire x1="-2.79" y1="4.25" x2="-2.79" y2="-4.25" width="0.1778" layer="21"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="2.79" y1="4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="4.25" x2="2.79" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="-4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<pad name="2" x="-1.27" y="-5.85" drill="0.8"/>
<pad name="1" x="1.27" y="-5.85" drill="0.8"/>
</package>
<package name="SAMTECH_FTSH-105-01">
<description>&lt;h3&gt;ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.05"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.samtec.com/ftppub/cpdf/FTSH-1XX-XX-XXX-DV-XXX-MKT.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CORTEX_DEBUG&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<rectangle x1="-0.2032" y1="1.7145" x2="0.2032" y2="2.921" layer="51"/>
<rectangle x1="1.0668" y1="1.7145" x2="1.4732" y2="2.921" layer="51"/>
<rectangle x1="2.3368" y1="1.7145" x2="2.7432" y2="2.921" layer="51"/>
<rectangle x1="-1.4732" y1="1.7145" x2="-1.0668" y2="2.921" layer="51"/>
<rectangle x1="-2.7432" y1="1.7145" x2="-2.3368" y2="2.921" layer="51"/>
<rectangle x1="-0.2032" y1="-2.921" x2="0.2032" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-1.4732" y1="-2.921" x2="-1.0668" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-2.7432" y1="-2.921" x2="-2.3368" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="1.0668" y1="-2.921" x2="1.4732" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="2.3368" y1="-2.921" x2="2.7432" y2="-1.7145" layer="51" rot="R180"/>
<smd name="6" x="0" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="8" x="1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="10" x="2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="4" x="-1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="2" x="-2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="1" x="-2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="3" x="-1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="5" x="0" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="7" x="1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="9" x="2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<text x="-1.3462" y="0.4572" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.7018" y="-0.9652" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.8" y1="1.24" x2="0.8" y2="1.24" width="0.127" layer="21"/>
<wire x1="-6.3" y1="2.5" x2="-6.3" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-6.3" y1="-2.5" x2="6.3" y2="-2.5" width="0.127" layer="51"/>
<wire x1="6.3" y1="-2.5" x2="6.3" y2="2.5" width="0.127" layer="51"/>
<wire x1="6.3" y1="2.5" x2="-6.3" y2="2.5" width="0.127" layer="51"/>
<wire x1="-5.3" y1="1.6" x2="-5.3" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-5.3" y1="-1.6" x2="5.3" y2="-1.6" width="0.127" layer="51"/>
<wire x1="5.3" y1="-1.6" x2="5.3" y2="1.6" width="0.127" layer="51"/>
<wire x1="5.3" y1="1.6" x2="-5.3" y2="1.6" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.7" x2="-3.2" y2="-1.7" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.7" x2="3.2" y2="1.7" width="0.127" layer="51"/>
<wire x1="3.2" y1="-1.7" x2="-3.2" y2="-1.7" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.7" x2="3.2" y2="-1.7" width="0.127" layer="51"/>
</package>
<package name="2X5-PTH-1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.635" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.635" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.635" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="51"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_02">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CORTEX_DEBUG">
<description>&lt;h3&gt;Cortex Debug Connector&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="VCC" x="-15.24" y="5.08" length="short"/>
<pin name="GND@3" x="-15.24" y="2.54" length="short"/>
<pin name="GND@5" x="-15.24" y="0" length="short"/>
<pin name="KEY" x="-15.24" y="-2.54" length="short"/>
<pin name="GNDDTCT" x="-15.24" y="-5.08" length="short"/>
<pin name="!RESET" x="17.78" y="-5.08" length="short" rot="R180"/>
<pin name="NC/TDI" x="17.78" y="-2.54" length="short" rot="R180"/>
<pin name="SWO/TDO" x="17.78" y="0" length="short" rot="R180"/>
<pin name="SWDCLK/TCK" x="17.78" y="2.54" length="short" rot="R180"/>
<pin name="SWDIO/TMS" x="17.78" y="5.08" length="short" rot="R180"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="7.62" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<text x="-12.7" y="7.874" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-12.7" y="-9.906" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_02" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="CONN_02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_SKU" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL_POKEHOME" package="1X02_POKEHOME">
<connects>
<connect gate="G$1" pin="1" pad="P1 P3"/>
<connect gate="G$1" pin="2" pad="P2 P4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13512"/>
</technology>
</technologies>
</device>
<device name="PTH_RA_FEMALE" package="1X02_RA_PTH_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13700"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CORTEX_DEBUG" prefix="J">
<description>&lt;h3&gt;Cortex Debug Connector - 10 pin&lt;/h3&gt;
&lt;p&gt;Supports JTAG debug, Serial Wire debug, and Serial Wire Viewer.
PTH and SMD connector options available.&lt;/p&gt;
&lt;p&gt; &lt;ul&gt;&lt;a href=”http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf”&gt;General Connector Information&lt;/a&gt;
&lt;p&gt;&lt;b&gt; Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”http://www.digikey.com/product-detail/en/cnc-tech/3220-10-0100-00/1175-1627-ND/3883661”&gt;PTH Connector&lt;/a&gt; -via Digi-Key&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13229”&gt;SparkFun PSoc&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13810”&gt;SparkFun T&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CORTEX_DEBUG" x="0" y="0"/>
</gates>
<devices>
<device name="_SMD" package="SAMTECH_FTSH-105-01">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PTH" package="2X5-PTH-1.27MM">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="borkedlabs-passives">
<packages>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="744777920-INDUCTOR">
<smd name="P$1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="P$2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="-4" y1="0" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="4" y1="3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-4" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-4" y1="-3" x2="-4" y2="0" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-4" x2="4" y2="4" layer="39"/>
<text x="5.08" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="SPM6530-IND">
<smd name="1" x="0" y="2.775" dx="3.4" dy="1.85" layer="1"/>
<smd name="2" x="0" y="-2.775" dx="3.4" dy="1.85" layer="1"/>
<wire x1="-3.25" y1="3.85" x2="-3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.85" x2="3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.85" x2="3.25" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.85" x2="-3.25" y2="3.85" width="0.127" layer="21"/>
<text x="3.81" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="IHLP-5050FD-01-IND">
<smd name="1" x="0" y="5.4102" dx="4.953" dy="2.9464" layer="1"/>
<smd name="2" x="0" y="-5.4102" dx="4.953" dy="2.9464" layer="1"/>
<wire x1="6.4516" y1="6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="6.4516" y1="6.604" x2="3.81" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.604" x2="-6.4516" y2="6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="6.604" x2="-6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="-6.604" x2="-3.81" y2="-6.604" width="0.127" layer="21"/>
<text x="5.08" y="7.62" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="-8.89" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="7443340330-IND">
<smd name="P$1" x="0" y="3.35" dx="3" dy="2.3" layer="1"/>
<smd name="P$2" x="0" y="-3.35" dx="3" dy="2.3" layer="1"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<text x="3" y="5" size="1.016" layer="25">&gt;NAME</text>
<text x="3" y="-6" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.15" y1="2.95" x2="1.15" y2="4.45" layer="51"/>
<rectangle x1="-1.15" y1="-4.45" x2="1.15" y2="-2.95" layer="51"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="8X8-IND">
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="744029100-IND">
<smd name="1" x="0" y="1.1" dx="3.2" dy="1" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="3.2" dy="1" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447709470-IND">
<smd name="1" x="0" y="4.95" dx="5.4" dy="2.9" layer="1"/>
<smd name="2" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<text x="-7" y="8" size="1.27" layer="25">&gt;NAME</text>
<text x="-7" y="-9" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447789002-IND">
<smd name="1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="6.35" y="-5.08" size="1.27" layer="97" rot="R90">&gt;PACKAGE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-744777920" package="744777920-INDUCTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
</technology>
</technologies>
</device>
<device name="-SPM6530" package="SPM6530-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IHLP-5050FD-01" package="IHLP-5050FD-01-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7443340330" package="7443340330-IND">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="7443340330"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
</technology>
</technologies>
</device>
<device name="-744778002" package="8X8-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-744029100" package="744029100-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447709470" package="7447709470-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447789002" package="7447789002-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;, 5 lead</description>
<wire x1="-1.544" y1="0.713" x2="1.544" y2="0.713" width="0.1524" layer="21"/>
<wire x1="1.544" y1="0.713" x2="1.544" y2="-0.712" width="0.1524" layer="21"/>
<wire x1="1.544" y1="-0.712" x2="-1.544" y2="-0.712" width="0.1524" layer="21"/>
<wire x1="-1.544" y1="-0.712" x2="-1.544" y2="0.713" width="0.1524" layer="21"/>
<smd name="5" x="-0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<text x="-1.778" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.048" y="-1.778" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
</package>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="PWRPAD_M3-PEM-MOUNT">
<pad name="P$1" x="0" y="0" drill="4.1" diameter="6.2" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.6"/>
<vertex x="3" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.59705625" y="-1.72131875"/>
<vertex x="1.748528125" y="-2.569846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.569846875"/>
<vertex x="-2.55269375" y="-1.72131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3" width="0.125" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="VREG-AP2112">
<pin name="VIN" x="-12.7" y="2.54" length="middle"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="PWRPAD_M3-PEM-MOUNT">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VREG-AP2112" prefix="U">
<gates>
<gate name="G$1" symbol="VREG-AP2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="RJ12-6-SMT">
<smd name="P$1" x="-3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$7" x="-5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<smd name="P$8" x="5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="-6.604" y1="16.891" x2="6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="6.604" y1="16.891" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="3" y1="0" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="-3" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="-6.5" y1="16.8" x2="-6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="-6.5" y1="16.3" x2="-6" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6" y1="16.8" x2="-6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.8" x2="6" y2="16.8" width="0.127" layer="21"/>
<wire x1="6" y1="16.8" x2="6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.3" x2="6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.1" x2="-6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.6" x2="-6" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6" y1="0.1" x2="-6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.1" x2="6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.6" x2="6" y2="0.1" width="0.127" layer="21"/>
<wire x1="6" y1="0.1" x2="6.5" y2="0.1" width="0.127" layer="21"/>
<text x="-6" y="0.5" size="0.8128" layer="21" font="vector">stlb</text>
<text x="4" y="0.5" size="0.8128" layer="21" font="vector">stlr</text>
<wire x1="-5.25" y1="-0.75" x2="-5.25" y2="0.25" width="0.127" layer="21"/>
<wire x1="5.25" y1="-0.75" x2="5.25" y2="0.25" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-1.5" x2="-3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-1.5" x2="-2" y2="5" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="2" y2="5" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="3.25" y1="-1.5" x2="6.5" y2="-1.5" width="0.127" layer="51"/>
</package>
<package name="RJ12-6-SMT-WIDE">
<smd name="P$1" x="-3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$7" x="-6.604" y="6.35" dx="5.207" dy="5.08" layer="1" rot="R90"/>
<smd name="P$8" x="6.604" y="6.35" dx="5.207" dy="5.08" layer="1" rot="R90"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="-6.604" y1="16.891" x2="6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="6.604" y1="16.891" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
</package>
<package name="RJ12-6-SMT-TOPENTRY">
<smd name="P$1" x="-3.175" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$7" x="0" y="-0.25" dx="8.8" dy="4.5" layer="1"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="15.791" width="0.127" layer="51"/>
<wire x1="-6.604" y1="15.791" x2="6.604" y2="15.791" width="0.127" layer="51"/>
<wire x1="6.604" y1="15.791" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
<wire x1="-6.5" y1="15.7" x2="-6.5" y2="15.2" width="0.127" layer="21"/>
<wire x1="-6.5" y1="15.2" x2="-6" y2="15.7" width="0.127" layer="21"/>
<wire x1="-6" y1="15.7" x2="-6.5" y2="15.7" width="0.127" layer="21"/>
<wire x1="6.5" y1="15.7" x2="6" y2="15.7" width="0.127" layer="21"/>
<wire x1="6" y1="15.7" x2="6.5" y2="15.2" width="0.127" layer="21"/>
<wire x1="6.5" y1="15.2" x2="6.5" y2="15.7" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.1" x2="-6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.6" x2="-6" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6" y1="0.1" x2="-6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.1" x2="6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.6" x2="6" y2="0.1" width="0.127" layer="21"/>
<wire x1="6" y1="0.1" x2="6.5" y2="0.1" width="0.127" layer="21"/>
<text x="-6" y="14.25" size="0.8128" layer="21" font="vector">stlb</text>
<text x="4.25" y="14.25" size="0.8128" layer="21" font="vector">stlr</text>
<wire x1="-5.25" y1="15.5" x2="-5.25" y2="16.5" width="0.127" layer="21"/>
<wire x1="5.25" y1="15.5" x2="5.25" y2="16.5" width="0.127" layer="21"/>
<wire x1="-5.75" y1="12" x2="5.75" y2="12" width="0.127" layer="51"/>
<wire x1="5.75" y1="12" x2="5.75" y2="4.25" width="0.127" layer="51"/>
<wire x1="5.75" y1="4.25" x2="2" y2="4.25" width="0.127" layer="51"/>
<wire x1="2" y1="4.25" x2="2" y2="2.25" width="0.127" layer="51"/>
<wire x1="2" y1="2.25" x2="-2" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2" y1="2.25" x2="-2" y2="4.25" width="0.127" layer="51"/>
<wire x1="-2" y1="4.25" x2="-5.75" y2="4.25" width="0.127" layer="51"/>
<wire x1="-5.75" y1="4.25" x2="-5.75" y2="12" width="0.127" layer="51"/>
</package>
<package name="RJ12-6-SMT-NOSILK">
<smd name="P$1" x="-3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$7" x="-5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<smd name="P$8" x="5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="-6.604" y1="16.891" x2="6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="6.604" y1="16.891" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="3" y1="0" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="-3" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="-6.5" y1="16.8" x2="-6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="-6.5" y1="16.3" x2="-6" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6" y1="16.8" x2="-6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.8" x2="6" y2="16.8" width="0.127" layer="21"/>
<wire x1="6" y1="16.8" x2="6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.3" x2="6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.1" x2="-6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.6" x2="-6" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6" y1="0.1" x2="-6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.1" x2="6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.6" x2="6" y2="0.1" width="0.127" layer="21"/>
<wire x1="6" y1="0.1" x2="6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-1.5" x2="-3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-1.5" x2="-2" y2="5" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="2" y2="5" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="3.25" y1="-1.5" x2="6.5" y2="-1.5" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RJ12-6">
<pin name="1" x="-5.08" y="12.7" length="middle"/>
<pin name="3" x="-5.08" y="7.62" length="middle"/>
<pin name="5" x="-5.08" y="2.54" length="middle"/>
<pin name="2" x="-5.08" y="10.16" length="middle"/>
<pin name="4" x="-5.08" y="5.08" length="middle"/>
<pin name="6" x="-5.08" y="0" length="middle"/>
<wire x1="0" y1="15.24" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<text x="0" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="17.78" size="1.778" layer="96">RJ12</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RJ12-6-SMT" prefix="J">
<gates>
<gate name="G$1" symbol="RJ12-6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RJ12-6-SMT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WIDE" package="RJ12-6-SMT-WIDE">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOP" package="RJ12-6-SMT-TOPENTRY">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NOSILK" package="RJ12-6-SMT-NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="atsam" deviceset="ATSAMS70N-TQFP100" device=""/>
<part name="S1" library="fab" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="C14" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C10" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C8" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C5" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C3" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C15" library="borkedlabs-passives" deviceset="CAP" device="1206" value="4.7uF"/>
<part name="C11" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C9" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C6" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C4" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C2" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C1" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="+3V31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L3" library="borkedlabs-passives" deviceset="INDUCTOR" device="-0805" value="0.75nH"/>
<part name="L1" library="borkedlabs-passives" deviceset="INDUCTOR" device="-0805" value="0.75uH"/>
<part name="C16" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C7" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="L2" library="borkedlabs-passives" deviceset="INDUCTOR" device="-0805" value="10uH 50mA"/>
<part name="C13" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C12" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J1" library="SparkFun-Connectors" deviceset="CONN_02" device="1X02_NO_SILK"/>
<part name="+3V33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="X1" library="fab" deviceset="MICRO-USB" device="_ORIG"/>
<part name="C17" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C19" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="J5" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J4" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J3" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J6" library="power" deviceset="PWRPAD" device="M3"/>
<part name="R1" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="+3V36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C18" library="borkedlabs-passives" deviceset="CAP" device="0805" value="1uF"/>
<part name="U2" library="power" deviceset="VREG-AP2112" device=""/>
<part name="S2" library="fab" deviceset="SLIDE-SWITCH" device=""/>
<part name="D13" library="lights" deviceset="LED" device="0805"/>
<part name="D14" library="lights" deviceset="LED" device="0805"/>
<part name="R14" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="R15" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D1" library="lights" deviceset="LED" device="0805"/>
<part name="D2" library="lights" deviceset="LED" device="0805"/>
<part name="R2" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R3" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="J7" library="connector" deviceset="RJ12-6-SMT" device=""/>
<part name="+3V317" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C20" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D3" library="lights" deviceset="LED" device="0805"/>
<part name="D4" library="lights" deviceset="LED" device="0805"/>
<part name="R4" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R5" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="J8" library="connector" deviceset="RJ12-6-SMT" device=""/>
<part name="+3V318" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C21" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D5" library="lights" deviceset="LED" device="0805"/>
<part name="D6" library="lights" deviceset="LED" device="0805"/>
<part name="R6" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R7" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="J9" library="connector" deviceset="RJ12-6-SMT" device=""/>
<part name="+3V319" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C22" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V310" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D7" library="lights" deviceset="LED" device="0805"/>
<part name="D8" library="lights" deviceset="LED" device="0805"/>
<part name="R8" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R9" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="J10" library="connector" deviceset="RJ12-6-SMT" device=""/>
<part name="+3V320" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C23" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V311" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D9" library="lights" deviceset="LED" device="0805"/>
<part name="D10" library="lights" deviceset="LED" device="0805"/>
<part name="R10" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R11" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="J11" library="connector" deviceset="RJ12-6-SMT" device=""/>
<part name="+3V321" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C24" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V312" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V315" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V316" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D12" library="lights" deviceset="LED" device="0805"/>
<part name="R13" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="+3V314" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D11" library="lights" deviceset="LED" device="0805"/>
<part name="R12" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="+3V313" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J2" library="SparkFun-Connectors" deviceset="CORTEX_DEBUG" device="_SMD"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="63.5" y="124.46"/>
<instance part="S1" gate="G$1" x="58.42" y="-10.16" rot="R270"/>
<instance part="C14" gate="G$1" x="17.78" y="177.8"/>
<instance part="C10" gate="G$1" x="7.62" y="177.8"/>
<instance part="C8" gate="G$1" x="-2.54" y="177.8"/>
<instance part="C5" gate="G$1" x="-12.7" y="177.8"/>
<instance part="C3" gate="G$1" x="-22.86" y="177.8"/>
<instance part="C15" gate="G$1" x="17.78" y="162.56"/>
<instance part="C11" gate="G$1" x="7.62" y="162.56"/>
<instance part="C9" gate="G$1" x="-2.54" y="162.56"/>
<instance part="C6" gate="G$1" x="-12.7" y="162.56"/>
<instance part="C4" gate="G$1" x="-22.86" y="162.56"/>
<instance part="C2" gate="G$1" x="-33.02" y="162.56"/>
<instance part="C1" gate="G$1" x="-33.02" y="177.8"/>
<instance part="+3V31" gate="G$1" x="-43.18" y="182.88" rot="R90"/>
<instance part="GND1" gate="1" x="-43.18" y="175.26" rot="R270"/>
<instance part="GND2" gate="1" x="-43.18" y="160.02" rot="R270"/>
<instance part="L3" gate="G$1" x="7.62" y="149.86" rot="R270"/>
<instance part="L1" gate="G$1" x="-15.24" y="139.7" rot="R270"/>
<instance part="C16" gate="G$1" x="17.78" y="144.78"/>
<instance part="C7" gate="G$1" x="-5.08" y="134.62"/>
<instance part="GND6" gate="1" x="12.7" y="142.24" rot="R270"/>
<instance part="GND4" gate="1" x="-10.16" y="132.08" rot="R270"/>
<instance part="L2" gate="G$1" x="7.62" y="193.04" rot="R270"/>
<instance part="C13" gate="G$1" x="17.78" y="187.96"/>
<instance part="C12" gate="G$1" x="17.78" y="203.2"/>
<instance part="GND5" gate="1" x="12.7" y="200.66" rot="R270"/>
<instance part="+3V34" gate="G$1" x="12.7" y="208.28" rot="R90"/>
<instance part="+3V32" gate="G$1" x="-10.16" y="193.04" rot="R90"/>
<instance part="GND3" gate="1" x="-10.16" y="187.96" rot="R270"/>
<instance part="GND7" gate="1" x="12.7" y="114.3" rot="R270"/>
<instance part="GND11" gate="1" x="81.28" y="-10.16" rot="R90"/>
<instance part="J1" gate="G$1" x="-5.08" y="71.12"/>
<instance part="+3V33" gate="G$1" x="7.62" y="71.12" rot="R270"/>
<instance part="X1" gate="G$1" x="15.24" y="241.3" rot="R270"/>
<instance part="C17" gate="G$1" x="48.26" y="236.22"/>
<instance part="GND9" gate="1" x="43.18" y="248.92" rot="R180"/>
<instance part="+3V37" gate="G$1" x="121.92" y="238.76" rot="R270"/>
<instance part="GND10" gate="1" x="68.58" y="220.98"/>
<instance part="C19" gate="G$1" x="91.44" y="236.22"/>
<instance part="J5" gate="G$1" x="109.22" y="17.78"/>
<instance part="J4" gate="G$1" x="109.22" y="22.86"/>
<instance part="J3" gate="G$1" x="109.22" y="27.94"/>
<instance part="J6" gate="G$1" x="109.22" y="12.7"/>
<instance part="R1" gate="G$1" x="43.18" y="-5.08" rot="R90"/>
<instance part="+3V36" gate="G$1" x="43.18" y="5.08"/>
<instance part="C18" gate="G$1" x="83.82" y="236.22"/>
<instance part="U2" gate="G$1" x="68.58" y="238.76"/>
<instance part="S2" gate="G$1" x="106.68" y="238.76" rot="R180"/>
<instance part="D13" gate="G$1" x="215.9" y="35.56" rot="R270"/>
<instance part="D14" gate="G$1" x="215.9" y="22.86" rot="R270"/>
<instance part="R14" gate="G$1" x="205.74" y="35.56"/>
<instance part="R15" gate="G$1" x="205.74" y="22.86"/>
<instance part="D1" gate="G$1" x="215.9" y="205.74" rot="R270"/>
<instance part="D2" gate="G$1" x="215.9" y="193.04" rot="R270"/>
<instance part="R2" gate="G$1" x="205.74" y="205.74"/>
<instance part="R3" gate="G$1" x="205.74" y="193.04"/>
<instance part="J7" gate="G$1" x="172.72" y="193.04"/>
<instance part="+3V317" gate="G$1" x="231.14" y="198.12" rot="R270"/>
<instance part="C20" gate="G$1" x="144.78" y="195.58"/>
<instance part="GND12" gate="1" x="139.7" y="200.66" rot="R270"/>
<instance part="+3V38" gate="G$1" x="129.54" y="193.04" rot="R90"/>
<instance part="D3" gate="G$1" x="215.9" y="177.8" rot="R270"/>
<instance part="D4" gate="G$1" x="215.9" y="165.1" rot="R270"/>
<instance part="R4" gate="G$1" x="205.74" y="177.8"/>
<instance part="R5" gate="G$1" x="205.74" y="165.1"/>
<instance part="J8" gate="G$1" x="172.72" y="165.1"/>
<instance part="+3V318" gate="G$1" x="231.14" y="170.18" rot="R270"/>
<instance part="C21" gate="G$1" x="144.78" y="167.64"/>
<instance part="GND13" gate="1" x="139.7" y="172.72" rot="R270"/>
<instance part="+3V39" gate="G$1" x="129.54" y="165.1" rot="R90"/>
<instance part="D5" gate="G$1" x="215.9" y="149.86" rot="R270"/>
<instance part="D6" gate="G$1" x="215.9" y="137.16" rot="R270"/>
<instance part="R6" gate="G$1" x="205.74" y="149.86"/>
<instance part="R7" gate="G$1" x="205.74" y="137.16"/>
<instance part="J9" gate="G$1" x="172.72" y="137.16"/>
<instance part="+3V319" gate="G$1" x="231.14" y="142.24" rot="R270"/>
<instance part="C22" gate="G$1" x="144.78" y="139.7"/>
<instance part="GND14" gate="1" x="139.7" y="144.78" rot="R270"/>
<instance part="+3V310" gate="G$1" x="129.54" y="137.16" rot="R90"/>
<instance part="D7" gate="G$1" x="215.9" y="121.92" rot="R270"/>
<instance part="D8" gate="G$1" x="215.9" y="109.22" rot="R270"/>
<instance part="R8" gate="G$1" x="205.74" y="121.92"/>
<instance part="R9" gate="G$1" x="205.74" y="109.22"/>
<instance part="J10" gate="G$1" x="172.72" y="109.22"/>
<instance part="+3V320" gate="G$1" x="231.14" y="114.3" rot="R270"/>
<instance part="C23" gate="G$1" x="144.78" y="111.76"/>
<instance part="GND15" gate="1" x="139.7" y="116.84" rot="R270"/>
<instance part="+3V311" gate="G$1" x="129.54" y="109.22" rot="R90"/>
<instance part="D9" gate="G$1" x="215.9" y="93.98" rot="R270"/>
<instance part="D10" gate="G$1" x="215.9" y="81.28" rot="R270"/>
<instance part="R10" gate="G$1" x="205.74" y="93.98"/>
<instance part="R11" gate="G$1" x="205.74" y="81.28"/>
<instance part="J11" gate="G$1" x="172.72" y="81.28"/>
<instance part="+3V321" gate="G$1" x="231.14" y="86.36" rot="R270"/>
<instance part="C24" gate="G$1" x="144.78" y="83.82"/>
<instance part="GND16" gate="1" x="139.7" y="88.9" rot="R270"/>
<instance part="+3V312" gate="G$1" x="129.54" y="81.28" rot="R90"/>
<instance part="+3V315" gate="G$1" x="226.06" y="35.56" rot="R270"/>
<instance part="+3V316" gate="G$1" x="226.06" y="22.86" rot="R270"/>
<instance part="D12" gate="G$1" x="215.9" y="48.26" rot="R270"/>
<instance part="R13" gate="G$1" x="205.74" y="48.26"/>
<instance part="+3V314" gate="G$1" x="226.06" y="48.26" rot="R270"/>
<instance part="D11" gate="G$1" x="215.9" y="60.96" rot="R270"/>
<instance part="R12" gate="G$1" x="205.74" y="60.96"/>
<instance part="+3V313" gate="G$1" x="226.06" y="60.96" rot="R270"/>
<instance part="GND17" gate="1" x="187.96" y="60.96" rot="R270"/>
<instance part="J2" gate="G$1" x="55.88" y="25.4"/>
<instance part="GND8" gate="1" x="27.94" y="20.32" rot="R270"/>
<instance part="+3V35" gate="G$1" x="27.94" y="30.48" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="VDDCORE" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDOUT"/>
<wire x1="30.48" y1="167.64" x2="27.94" y2="167.64" width="0.1524" layer="91"/>
<wire x1="27.94" y1="167.64" x2="27.94" y2="162.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDCORE1"/>
<wire x1="27.94" y1="162.56" x2="30.48" y2="162.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDCORE2"/>
<wire x1="30.48" y1="160.02" x2="27.94" y2="160.02" width="0.1524" layer="91"/>
<wire x1="27.94" y1="160.02" x2="27.94" y2="162.56" width="0.1524" layer="91"/>
<junction x="27.94" y="162.56"/>
<pinref part="U1" gate="G$1" pin="VDDCORE3"/>
<wire x1="30.48" y1="157.48" x2="27.94" y2="157.48" width="0.1524" layer="91"/>
<wire x1="27.94" y1="157.48" x2="27.94" y2="160.02" width="0.1524" layer="91"/>
<junction x="27.94" y="160.02"/>
<pinref part="U1" gate="G$1" pin="VDDCORE4"/>
<wire x1="30.48" y1="154.94" x2="27.94" y2="154.94" width="0.1524" layer="91"/>
<wire x1="27.94" y1="154.94" x2="27.94" y2="157.48" width="0.1524" layer="91"/>
<junction x="27.94" y="157.48"/>
<wire x1="27.94" y1="167.64" x2="25.4" y2="167.64" width="0.1524" layer="91"/>
<junction x="27.94" y="167.64"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="25.4" y1="167.64" x2="17.78" y2="167.64" width="0.1524" layer="91"/>
<wire x1="17.78" y1="167.64" x2="7.62" y2="167.64" width="0.1524" layer="91"/>
<junction x="17.78" y="167.64"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="7.62" y1="167.64" x2="-2.54" y2="167.64" width="0.1524" layer="91"/>
<junction x="7.62" y="167.64"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="167.64" x2="-12.7" y2="167.64" width="0.1524" layer="91"/>
<junction x="-2.54" y="167.64"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="167.64" x2="-22.86" y2="167.64" width="0.1524" layer="91"/>
<junction x="-12.7" y="167.64"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="167.64" x2="-33.02" y2="167.64" width="0.1524" layer="91"/>
<junction x="-22.86" y="167.64"/>
<wire x1="25.4" y1="167.64" x2="25.4" y2="154.94" width="0.1524" layer="91"/>
<junction x="25.4" y="167.64"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="0" y1="149.86" x2="-2.54" y2="149.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="154.94" x2="-2.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="154.94" x2="-2.54" y2="149.86" width="0.1524" layer="91"/>
<label x="-2.54" y="154.94" size="1.778" layer="95"/>
<wire x1="-2.54" y1="149.86" x2="-2.54" y2="144.78" width="0.1524" layer="91"/>
<junction x="-2.54" y="149.86"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="139.7" x2="-25.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="144.78" x2="-25.4" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="144.78" x2="-25.4" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDIO1"/>
<wire x1="30.48" y1="182.88" x2="27.94" y2="182.88" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDIO2"/>
<wire x1="27.94" y1="182.88" x2="17.78" y2="182.88" width="0.1524" layer="91"/>
<wire x1="17.78" y1="182.88" x2="7.62" y2="182.88" width="0.1524" layer="91"/>
<wire x1="7.62" y1="182.88" x2="-2.54" y2="182.88" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="182.88" x2="-12.7" y2="182.88" width="0.1524" layer="91"/>
<wire x1="30.48" y1="180.34" x2="27.94" y2="180.34" width="0.1524" layer="91"/>
<wire x1="27.94" y1="180.34" x2="27.94" y2="182.88" width="0.1524" layer="91"/>
<junction x="27.94" y="182.88"/>
<pinref part="U1" gate="G$1" pin="VDDIO3"/>
<wire x1="30.48" y1="177.8" x2="27.94" y2="177.8" width="0.1524" layer="91"/>
<wire x1="27.94" y1="177.8" x2="27.94" y2="180.34" width="0.1524" layer="91"/>
<junction x="27.94" y="180.34"/>
<pinref part="U1" gate="G$1" pin="VDDIO4"/>
<wire x1="30.48" y1="175.26" x2="27.94" y2="175.26" width="0.1524" layer="91"/>
<wire x1="27.94" y1="175.26" x2="27.94" y2="177.8" width="0.1524" layer="91"/>
<junction x="27.94" y="177.8"/>
<pinref part="U1" gate="G$1" pin="VDDIN"/>
<wire x1="30.48" y1="170.18" x2="27.94" y2="170.18" width="0.1524" layer="91"/>
<wire x1="27.94" y1="170.18" x2="27.94" y2="175.26" width="0.1524" layer="91"/>
<junction x="27.94" y="175.26"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="17.78" y="182.88"/>
<pinref part="C10" gate="G$1" pin="1"/>
<junction x="7.62" y="182.88"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="-2.54" y="182.88"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="182.88" x2="-22.86" y2="182.88" width="0.1524" layer="91"/>
<junction x="-12.7" y="182.88"/>
<wire x1="-22.86" y1="182.88" x2="-33.02" y2="182.88" width="0.1524" layer="91"/>
<junction x="-22.86" y="182.88"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-33.02" y1="182.88" x2="-40.64" y2="182.88" width="0.1524" layer="91"/>
<junction x="-33.02" y="182.88"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="17.78" y1="208.28" x2="15.24" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VREFP"/>
<wire x1="30.48" y1="203.2" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<wire x1="17.78" y1="208.28" x2="27.94" y2="208.28" width="0.1524" layer="91"/>
<wire x1="27.94" y1="208.28" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<junction x="17.78" y="208.28"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDDUTMII"/>
<wire x1="30.48" y1="195.58" x2="0" y2="195.58" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="0" y1="195.58" x2="0" y2="193.04" width="0.1524" layer="91"/>
<wire x1="0" y1="193.04" x2="-7.62" y2="193.04" width="0.1524" layer="91"/>
<junction x="0" y="193.04"/>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="2.54" y1="71.12" x2="5.08" y2="71.12" width="0.1524" layer="91"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="43.18" y1="2.54" x2="43.18" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="2"/>
<pinref part="+3V37" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="238.76" x2="119.38" y2="238.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="220.98" y1="205.74" x2="223.52" y2="205.74" width="0.1524" layer="91"/>
<wire x1="223.52" y1="205.74" x2="223.52" y2="198.12" width="0.1524" layer="91"/>
<wire x1="223.52" y1="198.12" x2="228.6" y2="198.12" width="0.1524" layer="91"/>
<wire x1="223.52" y1="198.12" x2="223.52" y2="193.04" width="0.1524" layer="91"/>
<junction x="223.52" y="198.12"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="223.52" y1="193.04" x2="220.98" y2="193.04" width="0.1524" layer="91"/>
<pinref part="+3V317" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="6"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="167.64" y1="193.04" x2="144.78" y2="193.04" width="0.1524" layer="91"/>
<wire x1="144.78" y1="193.04" x2="134.62" y2="193.04" width="0.1524" layer="91"/>
<junction x="144.78" y="193.04"/>
<wire x1="134.62" y1="193.04" x2="134.62" y2="205.74" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="134.62" y1="205.74" x2="167.64" y2="205.74" width="0.1524" layer="91"/>
<pinref part="+3V38" gate="G$1" pin="+3V3"/>
<wire x1="132.08" y1="193.04" x2="134.62" y2="193.04" width="0.1524" layer="91"/>
<junction x="134.62" y="193.04"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="220.98" y1="177.8" x2="223.52" y2="177.8" width="0.1524" layer="91"/>
<wire x1="223.52" y1="177.8" x2="223.52" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="228.6" y2="170.18" width="0.1524" layer="91"/>
<wire x1="223.52" y1="170.18" x2="223.52" y2="165.1" width="0.1524" layer="91"/>
<junction x="223.52" y="170.18"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="223.52" y1="165.1" x2="220.98" y2="165.1" width="0.1524" layer="91"/>
<pinref part="+3V318" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="6"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="167.64" y1="165.1" x2="144.78" y2="165.1" width="0.1524" layer="91"/>
<wire x1="144.78" y1="165.1" x2="134.62" y2="165.1" width="0.1524" layer="91"/>
<junction x="144.78" y="165.1"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="177.8" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="1"/>
<wire x1="134.62" y1="177.8" x2="167.64" y2="177.8" width="0.1524" layer="91"/>
<pinref part="+3V39" gate="G$1" pin="+3V3"/>
<wire x1="132.08" y1="165.1" x2="134.62" y2="165.1" width="0.1524" layer="91"/>
<junction x="134.62" y="165.1"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="220.98" y1="149.86" x2="223.52" y2="149.86" width="0.1524" layer="91"/>
<wire x1="223.52" y1="149.86" x2="223.52" y2="142.24" width="0.1524" layer="91"/>
<wire x1="223.52" y1="142.24" x2="228.6" y2="142.24" width="0.1524" layer="91"/>
<wire x1="223.52" y1="142.24" x2="223.52" y2="137.16" width="0.1524" layer="91"/>
<junction x="223.52" y="142.24"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="223.52" y1="137.16" x2="220.98" y2="137.16" width="0.1524" layer="91"/>
<pinref part="+3V319" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="6"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="167.64" y1="137.16" x2="144.78" y2="137.16" width="0.1524" layer="91"/>
<wire x1="144.78" y1="137.16" x2="134.62" y2="137.16" width="0.1524" layer="91"/>
<junction x="144.78" y="137.16"/>
<wire x1="134.62" y1="137.16" x2="134.62" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="134.62" y1="149.86" x2="167.64" y2="149.86" width="0.1524" layer="91"/>
<pinref part="+3V310" gate="G$1" pin="+3V3"/>
<wire x1="132.08" y1="137.16" x2="134.62" y2="137.16" width="0.1524" layer="91"/>
<junction x="134.62" y="137.16"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="220.98" y1="121.92" x2="223.52" y2="121.92" width="0.1524" layer="91"/>
<wire x1="223.52" y1="121.92" x2="223.52" y2="114.3" width="0.1524" layer="91"/>
<wire x1="223.52" y1="114.3" x2="228.6" y2="114.3" width="0.1524" layer="91"/>
<wire x1="223.52" y1="114.3" x2="223.52" y2="109.22" width="0.1524" layer="91"/>
<junction x="223.52" y="114.3"/>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="223.52" y1="109.22" x2="220.98" y2="109.22" width="0.1524" layer="91"/>
<pinref part="+3V320" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="6"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="167.64" y1="109.22" x2="144.78" y2="109.22" width="0.1524" layer="91"/>
<wire x1="144.78" y1="109.22" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<junction x="144.78" y="109.22"/>
<wire x1="134.62" y1="109.22" x2="134.62" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="134.62" y1="121.92" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<pinref part="+3V311" gate="G$1" pin="+3V3"/>
<wire x1="132.08" y1="109.22" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<junction x="134.62" y="109.22"/>
</segment>
<segment>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="220.98" y1="93.98" x2="223.52" y2="93.98" width="0.1524" layer="91"/>
<wire x1="223.52" y1="93.98" x2="223.52" y2="86.36" width="0.1524" layer="91"/>
<wire x1="223.52" y1="86.36" x2="228.6" y2="86.36" width="0.1524" layer="91"/>
<wire x1="223.52" y1="86.36" x2="223.52" y2="81.28" width="0.1524" layer="91"/>
<junction x="223.52" y="86.36"/>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="223.52" y1="81.28" x2="220.98" y2="81.28" width="0.1524" layer="91"/>
<pinref part="+3V321" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="6"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="167.64" y1="81.28" x2="144.78" y2="81.28" width="0.1524" layer="91"/>
<wire x1="144.78" y1="81.28" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
<junction x="144.78" y="81.28"/>
<wire x1="134.62" y1="81.28" x2="134.62" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="134.62" y1="93.98" x2="167.64" y2="93.98" width="0.1524" layer="91"/>
<pinref part="+3V312" gate="G$1" pin="+3V3"/>
<wire x1="132.08" y1="81.28" x2="134.62" y2="81.28" width="0.1524" layer="91"/>
<junction x="134.62" y="81.28"/>
</segment>
<segment>
<pinref part="+3V316" gate="G$1" pin="+3V3"/>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="223.52" y1="22.86" x2="220.98" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V315" gate="G$1" pin="+3V3"/>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="223.52" y1="35.56" x2="220.98" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V314" gate="G$1" pin="+3V3"/>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="223.52" y1="48.26" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V313" gate="G$1" pin="+3V3"/>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="223.52" y1="60.96" x2="220.98" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="VCC"/>
<wire x1="40.64" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="17.78" y1="175.26" x2="7.62" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="7.62" y1="175.26" x2="-2.54" y2="175.26" width="0.1524" layer="91"/>
<junction x="7.62" y="175.26"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="175.26" x2="-12.7" y2="175.26" width="0.1524" layer="91"/>
<junction x="-2.54" y="175.26"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="175.26" x2="-22.86" y2="175.26" width="0.1524" layer="91"/>
<junction x="-12.7" y="175.26"/>
<wire x1="-22.86" y1="175.26" x2="-33.02" y2="175.26" width="0.1524" layer="91"/>
<junction x="-22.86" y="175.26"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-33.02" y1="175.26" x2="-40.64" y2="175.26" width="0.1524" layer="91"/>
<junction x="-33.02" y="175.26"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="17.78" y1="160.02" x2="7.62" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="7.62" y1="160.02" x2="-2.54" y2="160.02" width="0.1524" layer="91"/>
<junction x="7.62" y="160.02"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="160.02" x2="-12.7" y2="160.02" width="0.1524" layer="91"/>
<junction x="-2.54" y="160.02"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="160.02" x2="-22.86" y2="160.02" width="0.1524" layer="91"/>
<junction x="-12.7" y="160.02"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="160.02" x2="-33.02" y2="160.02" width="0.1524" layer="91"/>
<junction x="-22.86" y="160.02"/>
<wire x1="-33.02" y1="160.02" x2="-40.64" y2="160.02" width="0.1524" layer="91"/>
<junction x="-33.02" y="160.02"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="132.08" x2="-5.08" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="17.78" y1="142.24" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VREFN"/>
<wire x1="30.48" y1="200.66" x2="15.24" y2="200.66" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="2"/>
<junction x="15.24" y="200.66"/>
<wire x1="15.24" y1="200.66" x2="17.78" y2="200.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="17.78" y1="185.42" x2="0" y2="185.42" width="0.1524" layer="91"/>
<wire x1="0" y1="185.42" x2="0" y2="187.96" width="0.1524" layer="91"/>
<wire x1="0" y1="187.96" x2="-7.62" y2="187.96" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND1"/>
<wire x1="30.48" y1="127" x2="27.94" y2="127" width="0.1524" layer="91"/>
<wire x1="27.94" y1="127" x2="27.94" y2="124.46" width="0.1524" layer="91"/>
<wire x1="27.94" y1="124.46" x2="27.94" y2="121.92" width="0.1524" layer="91"/>
<wire x1="27.94" y1="121.92" x2="27.94" y2="119.38" width="0.1524" layer="91"/>
<wire x1="27.94" y1="119.38" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<wire x1="27.94" y1="116.84" x2="27.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="27.94" y1="114.3" x2="15.24" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND2"/>
<wire x1="30.48" y1="124.46" x2="27.94" y2="124.46" width="0.1524" layer="91"/>
<junction x="27.94" y="124.46"/>
<pinref part="U1" gate="G$1" pin="GND3"/>
<wire x1="30.48" y1="121.92" x2="27.94" y2="121.92" width="0.1524" layer="91"/>
<junction x="27.94" y="121.92"/>
<pinref part="U1" gate="G$1" pin="GND4"/>
<wire x1="30.48" y1="119.38" x2="27.94" y2="119.38" width="0.1524" layer="91"/>
<junction x="27.94" y="119.38"/>
<pinref part="U1" gate="G$1" pin="GND5"/>
<wire x1="30.48" y1="116.84" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<junction x="27.94" y="116.84"/>
<pinref part="U1" gate="G$1" pin="GND6"/>
<wire x1="30.48" y1="114.3" x2="27.94" y2="114.3" width="0.1524" layer="91"/>
<junction x="27.94" y="114.3"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="S1"/>
<wire x1="63.5" y1="-12.7" x2="66.04" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-12.7" x2="66.04" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="66.04" y1="-10.16" x2="78.74" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="20.32" y1="243.84" x2="43.18" y2="243.84" width="0.1524" layer="91"/>
<wire x1="43.18" y1="243.84" x2="43.18" y2="246.38" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="48.26" y1="233.68" x2="48.26" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="226.06" x2="68.58" y2="226.06" width="0.1524" layer="91"/>
<wire x1="68.58" y1="226.06" x2="68.58" y2="228.6" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="68.58" y1="223.52" x2="68.58" y2="226.06" width="0.1524" layer="91"/>
<junction x="68.58" y="226.06"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="91.44" y1="233.68" x2="83.82" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="83.82" y1="233.68" x2="83.82" y2="226.06" width="0.1524" layer="91"/>
<junction x="83.82" y="233.68"/>
<wire x1="83.82" y1="226.06" x2="68.58" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="4"/>
<wire x1="167.64" y1="198.12" x2="154.94" y2="198.12" width="0.1524" layer="91"/>
<wire x1="154.94" y1="198.12" x2="154.94" y2="200.66" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="3"/>
<wire x1="167.64" y1="200.66" x2="154.94" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="154.94" y1="200.66" x2="144.78" y2="200.66" width="0.1524" layer="91"/>
<junction x="154.94" y="200.66"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="144.78" y1="200.66" x2="142.24" y2="200.66" width="0.1524" layer="91"/>
<junction x="144.78" y="200.66"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="4"/>
<wire x1="167.64" y1="170.18" x2="154.94" y2="170.18" width="0.1524" layer="91"/>
<wire x1="154.94" y1="170.18" x2="154.94" y2="172.72" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="3"/>
<wire x1="167.64" y1="172.72" x2="154.94" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="154.94" y1="172.72" x2="144.78" y2="172.72" width="0.1524" layer="91"/>
<junction x="154.94" y="172.72"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="144.78" y1="172.72" x2="142.24" y2="172.72" width="0.1524" layer="91"/>
<junction x="144.78" y="172.72"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="4"/>
<wire x1="167.64" y1="142.24" x2="154.94" y2="142.24" width="0.1524" layer="91"/>
<wire x1="154.94" y1="142.24" x2="154.94" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="3"/>
<wire x1="167.64" y1="144.78" x2="154.94" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="154.94" y1="144.78" x2="144.78" y2="144.78" width="0.1524" layer="91"/>
<junction x="154.94" y="144.78"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="144.78" y1="144.78" x2="142.24" y2="144.78" width="0.1524" layer="91"/>
<junction x="144.78" y="144.78"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="4"/>
<wire x1="167.64" y1="114.3" x2="154.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="154.94" y1="114.3" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="3"/>
<wire x1="167.64" y1="116.84" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="154.94" y1="116.84" x2="144.78" y2="116.84" width="0.1524" layer="91"/>
<junction x="154.94" y="116.84"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="144.78" y1="116.84" x2="142.24" y2="116.84" width="0.1524" layer="91"/>
<junction x="144.78" y="116.84"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="4"/>
<wire x1="167.64" y1="86.36" x2="154.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="154.94" y1="86.36" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="3"/>
<wire x1="167.64" y1="88.9" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="154.94" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
<junction x="154.94" y="88.9"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="144.78" y1="88.9" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<junction x="144.78" y="88.9"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="200.66" y1="60.96" x2="190.5" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND@3"/>
<wire x1="40.64" y1="27.94" x2="38.1" y2="27.94" width="0.1524" layer="91"/>
<wire x1="38.1" y1="27.94" x2="38.1" y2="25.4" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="GND@5"/>
<wire x1="38.1" y1="25.4" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<wire x1="38.1" y1="25.4" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<junction x="38.1" y="25.4"/>
<pinref part="J2" gate="G$1" pin="GNDDTCT"/>
<wire x1="38.1" y1="20.32" x2="40.64" y2="20.32" width="0.1524" layer="91"/>
<wire x1="30.48" y1="20.32" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<junction x="38.1" y="20.32"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDPLL"/>
<wire x1="30.48" y1="149.86" x2="17.78" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="17.78" y1="149.86" x2="15.24" y2="149.86" width="0.1524" layer="91"/>
<junction x="17.78" y="149.86"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="139.7" x2="-5.08" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="139.7" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
<wire x1="27.94" y1="139.7" x2="27.94" y2="147.32" width="0.1524" layer="91"/>
<junction x="-5.08" y="139.7"/>
<pinref part="U1" gate="G$1" pin="VDDUTMIC"/>
<wire x1="27.94" y1="147.32" x2="30.48" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="15.24" y1="193.04" x2="17.78" y2="193.04" width="0.1524" layer="91"/>
<wire x1="17.78" y1="193.04" x2="27.94" y2="193.04" width="0.1524" layer="91"/>
<wire x1="27.94" y1="193.04" x2="27.94" y2="190.5" width="0.1524" layer="91"/>
<junction x="17.78" y="193.04"/>
<pinref part="U1" gate="G$1" pin="VDDPLLUSB"/>
<wire x1="27.94" y1="190.5" x2="30.48" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TRACESWO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB5/TDO/TRACESWO/TWCK1"/>
<wire x1="30.48" y1="60.96" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<label x="15.24" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SWO/TDO"/>
<wire x1="73.66" y1="25.4" x2="86.36" y2="25.4" width="0.1524" layer="91"/>
<label x="76.2" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB7/SWCLK/TCK"/>
<wire x1="30.48" y1="66.04" x2="15.24" y2="66.04" width="0.1524" layer="91"/>
<label x="15.24" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SWDCLK/TCK"/>
<wire x1="73.66" y1="27.94" x2="86.36" y2="27.94" width="0.1524" layer="91"/>
<label x="76.2" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB6/SWDIO/TMS"/>
<wire x1="30.48" y1="63.5" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
<label x="15.24" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SWDIO/TMS"/>
<wire x1="73.66" y1="30.48" x2="86.36" y2="30.48" width="0.1524" layer="91"/>
<label x="76.2" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="P"/>
<wire x1="53.34" y1="-10.16" x2="43.18" y2="-10.16" width="0.1524" layer="91"/>
<label x="33.02" y="-10.16" size="1.778" layer="95"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="43.18" y1="-10.16" x2="33.02" y2="-10.16" width="0.1524" layer="91"/>
<junction x="43.18" y="-10.16"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="NRST"/>
<wire x1="30.48" y1="81.28" x2="15.24" y2="81.28" width="0.1524" layer="91"/>
<label x="15.24" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="!RESET"/>
<wire x1="73.66" y1="20.32" x2="86.36" y2="20.32" width="0.1524" layer="91"/>
<label x="76.2" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="ERASE" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB12/ERASE"/>
<label x="15.24" y="73.66" size="1.778" layer="95"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="30.48" y1="73.66" x2="2.54" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<wire x1="20.32" y1="241.3" x2="48.26" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="VIN"/>
<wire x1="55.88" y1="241.3" x2="53.34" y2="241.3" width="0.1524" layer="91"/>
<junction x="48.26" y="241.3"/>
<pinref part="U2" gate="G$1" pin="EN"/>
<wire x1="53.34" y1="241.3" x2="48.26" y2="241.3" width="0.1524" layer="91"/>
<wire x1="55.88" y1="236.22" x2="53.34" y2="236.22" width="0.1524" layer="91"/>
<wire x1="53.34" y1="236.22" x2="53.34" y2="241.3" width="0.1524" layer="91"/>
<junction x="53.34" y="241.3"/>
</segment>
</net>
<net name="UP1RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA5/URXD1"/>
<wire x1="93.98" y1="190.5" x2="111.76" y2="190.5" width="0.1524" layer="91"/>
<label x="96.52" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="167.64" y1="175.26" x2="154.94" y2="175.26" width="0.1524" layer="91"/>
<label x="154.94" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP1TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA4/UTXD1/TWCK0"/>
<wire x1="93.98" y1="193.04" x2="111.76" y2="193.04" width="0.1524" layer="91"/>
<label x="96.52" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="5"/>
<wire x1="167.64" y1="167.64" x2="154.94" y2="167.64" width="0.1524" layer="91"/>
<label x="154.94" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP1STLR" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="200.66" y1="177.8" x2="190.5" y2="177.8" width="0.1524" layer="91"/>
<label x="190.5" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD20/SPI0_MISO"/>
<wire x1="93.98" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<label x="96.52" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP1STLB" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="200.66" y1="165.1" x2="190.5" y2="165.1" width="0.1524" layer="91"/>
<label x="190.5" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA11/PWMC0_PWMH0"/>
<wire x1="93.98" y1="177.8" x2="106.68" y2="177.8" width="0.1524" layer="91"/>
<label x="96.52" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP4RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD18/URXD4"/>
<wire x1="93.98" y1="78.74" x2="111.76" y2="78.74" width="0.1524" layer="91"/>
<label x="96.52" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="2"/>
<wire x1="167.64" y1="91.44" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
<label x="154.94" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP4TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD19/UTXD4"/>
<wire x1="93.98" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<label x="96.52" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="5"/>
<wire x1="167.64" y1="83.82" x2="154.94" y2="83.82" width="0.1524" layer="91"/>
<label x="154.94" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP2RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD25/URXD2"/>
<wire x1="93.98" y1="63.5" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<label x="96.52" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="167.64" y1="147.32" x2="154.94" y2="147.32" width="0.1524" layer="91"/>
<label x="154.94" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP2TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD26/UTXD2"/>
<wire x1="93.98" y1="60.96" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<label x="96.52" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="5"/>
<wire x1="167.64" y1="139.7" x2="154.94" y2="139.7" width="0.1524" layer="91"/>
<label x="154.94" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP0RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA9/URXD0"/>
<wire x1="93.98" y1="182.88" x2="111.76" y2="182.88" width="0.1524" layer="91"/>
<label x="96.52" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="167.64" y1="203.2" x2="154.94" y2="203.2" width="0.1524" layer="91"/>
<label x="154.94" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP0TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA10/UTXD0"/>
<wire x1="93.98" y1="180.34" x2="111.76" y2="180.34" width="0.1524" layer="91"/>
<label x="96.52" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="5"/>
<wire x1="167.64" y1="195.58" x2="154.94" y2="195.58" width="0.1524" layer="91"/>
<label x="154.94" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP3RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD28/URXD3/TWCK2"/>
<wire x1="93.98" y1="55.88" x2="111.76" y2="55.88" width="0.1524" layer="91"/>
<label x="96.52" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="167.64" y1="119.38" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<label x="154.94" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP3TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD30/UTXD3"/>
<wire x1="93.98" y1="53.34" x2="111.76" y2="53.34" width="0.1524" layer="91"/>
<label x="96.52" y="53.34" size="1.778" layer="95"/>
<label x="96.52" y="53.34" size="1.778" layer="95"/>
<label x="96.52" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="5"/>
<wire x1="167.64" y1="111.76" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<label x="154.94" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP0STLR" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="200.66" y1="205.74" x2="190.5" y2="205.74" width="0.1524" layer="91"/>
<label x="190.5" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA2"/>
<wire x1="93.98" y1="198.12" x2="106.68" y2="198.12" width="0.1524" layer="91"/>
<label x="96.52" y="198.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP2STLR" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA15/PWMC0_PWML3"/>
<wire x1="93.98" y1="167.64" x2="106.68" y2="167.64" width="0.1524" layer="91"/>
<label x="96.52" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="200.66" y1="149.86" x2="190.5" y2="149.86" width="0.1524" layer="91"/>
<label x="190.5" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP2STLB" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD27/SPI0_NPCS3/TWD2"/>
<wire x1="93.98" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<label x="96.52" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="200.66" y1="137.16" x2="190.5" y2="137.16" width="0.1524" layer="91"/>
<label x="190.5" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP0STLB" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="200.66" y1="193.04" x2="190.5" y2="193.04" width="0.1524" layer="91"/>
<label x="190.5" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD12/SPI0_NPCS2"/>
<wire x1="93.98" y1="93.98" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<label x="96.52" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP3STLR" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA22"/>
<wire x1="93.98" y1="149.86" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<label x="96.52" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="200.66" y1="121.92" x2="190.5" y2="121.92" width="0.1524" layer="91"/>
<label x="190.5" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP3STLB" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA8/XOUT32"/>
<wire x1="93.98" y1="185.42" x2="106.68" y2="185.42" width="0.1524" layer="91"/>
<label x="96.52" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="200.66" y1="109.22" x2="190.5" y2="109.22" width="0.1524" layer="91"/>
<label x="190.5" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP4STLR" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="200.66" y1="93.98" x2="190.5" y2="93.98" width="0.1524" layer="91"/>
<label x="190.5" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA18/AFE0_AD7"/>
<wire x1="93.98" y1="160.02" x2="106.68" y2="160.02" width="0.1524" layer="91"/>
<label x="96.52" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP4STLB" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="200.66" y1="81.28" x2="190.5" y2="81.28" width="0.1524" layer="91"/>
<label x="190.5" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA19/PWMC0_PWML0/AFE0_AD8"/>
<wire x1="93.98" y1="157.48" x2="106.68" y2="157.48" width="0.1524" layer="91"/>
<label x="96.52" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="91.44" y1="241.3" x2="83.82" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="VOUT"/>
<wire x1="81.28" y1="241.3" x2="83.82" y2="241.3" width="0.1524" layer="91"/>
<junction x="83.82" y="241.3"/>
<pinref part="S2" gate="G$1" pin="3"/>
<wire x1="91.44" y1="241.3" x2="101.6" y2="241.3" width="0.1524" layer="91"/>
<junction x="91.44" y="241.3"/>
</segment>
</net>
<net name="USBDM" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="20.32" y1="238.76" x2="35.56" y2="238.76" width="0.1524" layer="91"/>
<label x="25.4" y="238.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="USB_DM"/>
<wire x1="30.48" y1="91.44" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<label x="15.24" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="USBDP" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="20.32" y1="236.22" x2="35.56" y2="236.22" width="0.1524" layer="91"/>
<label x="25.4" y="236.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="USB_DP"/>
<wire x1="30.48" y1="93.98" x2="15.24" y2="93.98" width="0.1524" layer="91"/>
<label x="15.24" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="213.36" y1="193.04" x2="210.82" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="213.36" y1="205.74" x2="210.82" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="213.36" y1="165.1" x2="210.82" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="213.36" y1="177.8" x2="210.82" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="213.36" y1="137.16" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="213.36" y1="149.86" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="C"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="213.36" y1="109.22" x2="210.82" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="213.36" y1="121.92" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="C"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="213.36" y1="81.28" x2="210.82" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="C"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="213.36" y1="93.98" x2="210.82" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D13" gate="G$1" pin="C"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="213.36" y1="35.56" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D14" gate="G$1" pin="C"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="213.36" y1="22.86" x2="210.82" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LPACKET" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="200.66" y1="35.56" x2="190.5" y2="35.56" width="0.1524" layer="91"/>
<label x="190.5" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD14"/>
<wire x1="93.98" y1="88.9" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<label x="96.52" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LERROR" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="200.66" y1="22.86" x2="190.5" y2="22.86" width="0.1524" layer="91"/>
<label x="190.5" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD16"/>
<wire x1="93.98" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<label x="96.52" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="C"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="213.36" y1="48.26" x2="210.82" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="D11" gate="G$1" pin="C"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="213.36" y1="60.96" x2="210.82" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LTICKER" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="200.66" y1="48.26" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
<label x="190.5" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PD13"/>
<wire x1="93.98" y1="91.44" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<label x="96.52" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
